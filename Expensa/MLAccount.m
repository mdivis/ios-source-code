//
//  Account.m
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import "MLAccount.h"
#import "NSDictionary+Value.h"
#import "MLAccountRole.h"
@implementation MLAccount

+(instancetype)accountFromDisctionry:(NSDictionary*)dictionary{
    MLAccount*account = [[MLAccount alloc] init];
    account.name = [dictionary stringValueForKey:@"name"];
    account.accountId = [dictionary intValueForKey:@"id"];
    
    NSMutableArray*array = [NSMutableArray array];
    for (NSString*name in [dictionary objectForKey:@"roles"]) {
        [array addObject:[MLAccountRole roleWithName:name]];
    }
    
    if (array.count>0) {
        account.roles = [array copy];

    }
      
    for (MLAccountRole*role in account.roles) {
        if ([role.name isEqualToString:@"petrol"]) {
            account.isPetrol = YES;
            break;
        }
    }
    
    return account;

}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeInteger:self.accountId forKey:@"accountId"];
    [aCoder encodeObject:self.roles forKey:@"roles"];
    [aCoder encodeObject:self.user forKey:@"user"];
    [aCoder encodeDouble:self.balance forKey:@"balance"];
    [aCoder encodeBool:self.activeCard forKey:@"activeCard"];
    [aCoder encodeObject:self.teamName forKey:@"teamName"];
    [aCoder encodeObject:self.approveManager forKey:@"approveManager"];
    [aCoder encodeBool:self.isPetrol forKey:@"isPetrol"];
    [aCoder encodeObject:self.petrolUsers forKey:@"petrolUsers"];

}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if((self = [super init])) {
        self.name =  [aDecoder decodeObjectForKey:@"name"];
        self.teamName =  [aDecoder decodeObjectForKey:@"teamName"];
        self.approveManager =  [aDecoder decodeObjectForKey:@"approveManager"];

        self.accountId = [aDecoder decodeIntegerForKey:@"accountId"];
        self.roles = [aDecoder decodeObjectForKey:@"roles"];
        self.user = [aDecoder decodeObjectForKey:@"user"];
        self.balance = [aDecoder decodeDoubleForKey:@"balance"];
        self.activeCard = [aDecoder decodeBoolForKey:@"activeCard"];
        self.isPetrol = [aDecoder decodeBoolForKey:@"isPetrol"];
        self.petrolUsers = [aDecoder decodeObjectForKey:@"petrolUsers"];

        
    }
    return self;
}


@end
