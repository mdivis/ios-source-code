//
//  MLTransaction.m
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import "MLTransaction.h"
#import "NSDictionary+Value.h"

@implementation MLTransaction

+(instancetype)transactionFromDictionary:(NSDictionary*)dictionary{

    MLTransaction*transaction = [[MLTransaction alloc] init];
    transaction.transactionId = [dictionary intValueForKey:@"id" defaultValue:0];
    transaction.category = [dictionary stringValueForKey:@"category" defaultValue:@""];
    transaction.currency = [dictionary stringValueForKey:@"currency" defaultValue:@""];
    transaction.originalCurrency = [dictionary stringValueForKey:@"originalCurrency" defaultValue:@""];
    transaction.date = [dictionary dateValueForKey:@"date"];
    transaction.fxDifference = [dictionary doubleValueForKey:@"fxDifference" defaultValue:0];
    transaction.isManual = [dictionary boolValueForKey:@"isManual"  defaultValue:NO];
    transaction.merchant = [dictionary stringValueForKey:@"merchant" defaultValue:@""];
    transaction.note = [dictionary stringValueForKey:@"note" defaultValue:@""];
    transaction.primaryCostCenterId = [dictionary intValueForKey:@"primaryCostCenterId" defaultValue:0];
    transaction.secondaryCostCenterId = [dictionary intValueForKey:@"secondaryCostCenterId" defaultValue:0];
    transaction.originalPrice = [dictionary doubleValueForKey:@"originalPrice" defaultValue:0];
    transaction.priceWithVat = [dictionary doubleValueForKey:@"priceWithVat" defaultValue:0];
    transaction.privateSpendingAmount = [dictionary doubleValueForKey:@"privateSpendingAmount" defaultValue:0];
    transaction.state = [dictionary stringValueForKey:@"state" defaultValue:@""];
    transaction.isEditable = [dictionary boolValueForKey:@"isEditable" defaultValue:NO];
    
    if (dictionary[@"photo"] != [NSNull null]) {
        transaction.photo = [MLTransactionPhoto photoFromDictionary:dictionary];

    }
       

    return transaction;

}

-(void)deleteInBackgroundWithBlock:(void (^)(BOOL succeeded, NSError *error))block{

    NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    
    [MLApiManager sharedInstace].requestSerializer = [AFJSONRequestSerializer serializer];
    [[[MLApiManager sharedInstace] requestSerializer] setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];
    
    
    
    
    [[MLApiManager sharedInstace] DELETE:[NSString stringWithFormat:@"%@/%ld",ApiTransActions, (long)self.transactionId] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        
        block(YES, nil);
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"PUT  %@",error.description);
        block(NO, error);
        
    }];
    
}

-(void)updateInBackgroundWithBlock:(void (^)(BOOL succeeded, NSError *error))block{

    NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    NSDateFormatter*formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat  = @"yyyy-MM-dd'T'HH:mm:ssZ";
    
    NSMutableDictionary*dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setValue:self.category forKey:@"category"];
    
    if (self.isManual) {
        [dictionary setValue:self.currency forKey:@"currency"];
        [dictionary setValue:self.merchant forKey:@"merchant"];
        [dictionary setValue:[formatter stringFromDate:self.date] forKey:@"date"];
        [dictionary setValue:[@(self.priceWithVat) stringValue] forKey:@"priceWithVat"];

    }
    
  
    if (self.note.length>0) {
        [dictionary setValue:self.note forKey:@"note"];

    }
    if (self.primaryCostCenterId>0) {
        [dictionary setValue:[@(self.primaryCostCenterId) stringValue] forKey:@"primaryCostCenterId"];
        
    }
    if (self.secondaryCostCenterId>0) {
        [dictionary setValue:[@(self.secondaryCostCenterId)stringValue] forKey:@"secondaryCostCenterId"];
        
    }

        [dictionary setValue:[@(self.privateSpendingAmount)stringValue] forKey:@"privateSpendingAmount"];
        
    

    [MLApiManager sharedInstace].requestSerializer = [AFJSONRequestSerializer serializer];
    [[[MLApiManager sharedInstace] requestSerializer] setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];
    
    
    
    
    [[MLApiManager sharedInstace] PUT:[NSString stringWithFormat:@"%@/%ld",ApiTransActions, (long)self.transactionId] parameters:dictionary success:^(NSURLSessionDataTask *task, id responseObject) {
        
        
        block(YES, nil);
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"PUT  %@",error.description);
        block(NO, error);

    }];


}

-(void)createInBackgroundWithBlock:(void (^)(BOOL succeeded, NSError *error))block{
    
    NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    NSDateFormatter*formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat  = @"yyyy-MM-dd'T'HH:mm:ssZ";
    
    NSMutableDictionary*dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setValue:self.category forKey:@"category"];
    [dictionary setValue:self.currency forKey:@"currency"];
    [dictionary setValue:self.merchant forKey:@"merchant"];
    [dictionary setValue:[formatter stringFromDate:self.date] forKey:@"date"];
    [dictionary setValue:@(self.priceWithVat) forKey:@"priceWithVat"];
    
    if (self.note.length>0) {
        [dictionary setValue:self.note forKey:@"note"];
        
    }
    if (self.primaryCostCenterId>0) {
        [dictionary setValue:@(self.primaryCostCenterId) forKey:@"primaryCostCenterId"];
        
    }
    if (self.secondaryCostCenterId>0) {
        [dictionary setValue:@(self.secondaryCostCenterId) forKey:@"secondaryCostCenterId"];
        
    }
   
        [dictionary setValue:@(self.privateSpendingAmount) forKey:@"privateSpendingAmount"];
        
 
    
    [MLApiManager sharedInstace].requestSerializer = [AFJSONRequestSerializer serializer];
    [[[MLApiManager sharedInstace] requestSerializer] setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];
    
    
    
    
    [[MLApiManager sharedInstace] POST:[NSString stringWithFormat:@"%@",ApiTransActions] parameters:dictionary success:^(NSURLSessionDataTask *task, id responseObject) {
    
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        self.transactionId = [responseObject intValueForKey:@"transactionId" defaultValue:0];

        [realm commitWriteTransaction];

        
        
        block(YES, nil);
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"PUT  %@",error.description);
        block(NO, error);
        
    }];
    
    
}


+(void)fetchAllWithBlock:(void (^)(BOOL succeeded, NSError *error))block{
    
    
    [MLApiManager sharedInstace].requestSerializer = [AFHTTPRequestSerializer serializer];
    
    NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];

    
    [[[MLApiManager sharedInstace] requestSerializer] setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];
    [[MLApiManager sharedInstace] GET:ApiTransActions parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        
        
        NSArray * keys = [responseObject allKeys];
        
        NSMutableArray*allTransactions = [NSMutableArray array];
        NSMutableArray*originalTransactions = [NSMutableArray array];
        NSMutableArray*transactionsToAdd = [NSMutableArray array];
        NSMutableArray*transactionsToRemove = [NSMutableArray array];

        for (NSString*key in keys) {
            NSArray*transaction = [responseObject[key] objectForKey:@"transactions"];
            
            for (NSDictionary*trans in transaction) {
                [allTransactions addObject:[MLTransaction transactionFromDictionary:trans]];
            }
            
        }
        
        
        
        
        RLMResults*originaltrans = [MLTransaction allObjects];
        
        RLMRealm *realm = [RLMRealm defaultRealm];

        
        for (MLTransaction*curTransaction in originaltrans) {
            [originalTransactions addObject:curTransaction];
        }
        
        
        if (originalTransactions.count>0) {
            
            //ADD NEWONE
            for (MLTransaction*newTransaction in allTransactions) {
                
                BOOL shouldAdd = YES;
       
            
            for (MLTransaction*oldTransaction in originalTransactions) {
                
                if (newTransaction.transactionId==oldTransaction.transactionId) {
                    
                    [realm beginWriteTransaction];
                    oldTransaction.photo = newTransaction.photo;
                    oldTransaction.priceWithVat = newTransaction.priceWithVat;
                    oldTransaction.secondaryCostCenterId = newTransaction.secondaryCostCenterId;
                    oldTransaction.primaryCostCenterId = newTransaction.primaryCostCenterId;
                    oldTransaction.category = newTransaction.category;
                    oldTransaction.note = newTransaction.note;
                    oldTransaction.currency = newTransaction.currency;
                    oldTransaction.originalCurrency = newTransaction.originalCurrency;
                    oldTransaction.originalPrice = newTransaction.originalPrice;
                    oldTransaction.privateSpendingAmount = newTransaction.privateSpendingAmount;
                    oldTransaction.isEditable = newTransaction.isEditable;
                    oldTransaction.merchant = newTransaction.merchant;
                    
//                    oldTransaction.photo.fileData = [NSData data];
                    [realm commitWriteTransaction];
                    
                    
                    shouldAdd = NO;
                    break;
                }
              
                
            }
                
                if (shouldAdd) {
                    [transactionsToAdd addObject:newTransaction];
                }
                
                     }
            
            
            //DELETE NEWONE
            
             for (MLTransaction*oldTransaction in originalTransactions) {
                 
                 BOOL shouldDelete = YES;

                 for (MLTransaction*newTransaction in allTransactions) {

                    if (oldTransaction.transactionId==newTransaction.transactionId) {
                        shouldDelete = NO;
                        break;
                    }
                    
                    
                }
                
                if (shouldDelete) {
                    [transactionsToRemove addObject:oldTransaction];
                }
                
            }
            
            
            [realm beginWriteTransaction];
            [realm addObjects:transactionsToAdd];
            [realm deleteObjects:transactionsToRemove];
            [realm commitWriteTransaction];


            
            
            
        }
        else{
        
            
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            [realm addObjects:allTransactions];

            [realm commitWriteTransaction];
 
        
        }
        
     
        
        block(YES, nil);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        block(NO, error);
        
    }];
    
    
}

@end
