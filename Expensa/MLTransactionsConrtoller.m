//
//  ViewController.m
//  Expensa
//
//  Created by Pavel Nemecek on 27/03/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import "MLTransactionsConrtoller.h"
#import "SVProgressHUD.h"
#import "MLSession.h"
#import "SVProgressHUD.h"
#import "MLCostCenter.h"
#import "MLCurrency.h"
#import "MLCategory.h"
#import "MLTransaction.h"
#import "MLTransactionTableViewCell.h"
#import <Realm/Realm.h>
#import "MLTransactionDetailViewController.h"
#import "AbstractActionSheetPicker.h"
#import "ActionSheetStringPicker.h"
#import "ActionSheetDatePicker.h"
#import "BKPasscodeViewController.h"
#import "FXReachability.h"
#import "JDStatusBarNotification.h"
#import "MLAppDelegate.h"
#import "MLSection.h"
#import "UIFont+MOFonts.h"
#import "UIColor+MOColors.h"
#import "MLUser.h"
#import <Google/Analytics.h>

@interface MLTransactionsConrtoller ()<UITableViewDataSource, UITableViewDelegate, BKPasscodeViewControllerDelegate>
@property (nonatomic,weak) IBOutlet UITableView*tableView;
@property (nonatomic,strong) RLMResults*results;
@property (nonatomic,weak) IBOutlet UIButton*addButton;
@property (nonatomic,weak) IBOutlet UILabel*balanceLabel;
@property (nonatomic,strong) UIRefreshControl *refreshControl;

@property (nonatomic,weak) IBOutlet UILabel*emptyLabel;
@property(nonatomic,strong) NSMutableArray*sections;
@property(nonatomic,strong) NSTimer *timer;
@property (nonatomic,assign) NSInteger time;
@end

@implementation MLTransactionsConrtoller

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.emptyLabel.hidden = YES;
    self.time = 0;
    self.addButton.layer.cornerRadius = 18.0;
    [self.addButton setBackgroundColor: [UIColor colorFromHexCode:@"#2EAAFF" andAlpha:0.85]];


    [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 66, 0)];
    
    self.tableView.tableFooterView = [UIView new];
    
    NSUserDefaults*defaults = [NSUserDefaults standardUserDefaults];
    
    if (![defaults objectForKey:@"passcode"]) {
        
        BKPasscodeViewController *viewController = [[BKPasscodeViewController alloc] initWithNibName:nil bundle:nil];
        viewController.delegate = self;
        viewController.type = BKPasscodeViewControllerNewPasscodeType;
        // viewController.type = BKPasscodeViewControllerChangePasscodeType;    // for change
        // viewController.type = BKPasscodeViewControllerCheckPasscodeType;   // for authentication
        
        
        viewController.passcodeStyle = BKPasscodeInputViewNumericPasscodeStyle;
        // viewController.passcodeStyle = BKPasscodeInputViewNormalPasscodeStyle;    // for ASCII style passcode.
        
        // To supports Touch ID feature, set BKTouchIDManager instance to view controller.
        // It only supports iOS 8 or greater.
        viewController.touchIDManager = [[BKTouchIDManager alloc] initWithKeychainServiceName:@"com.expensa.keychain.pass"];
        viewController.touchIDManager.promptText = @"Pro ověření naskenujte otisk";   // You can set prompt text.
 
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
        [self presentViewController:navController animated:NO completion:nil];
        
    }
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshTransactions) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:self.refreshControl];
    
    
    // Start the notifier, which will cause the reachability object to retain itself!
    
    [self registerForPush];

    self.sections = [NSMutableArray array];
    
}




-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    
    
}

-(void)updateBalance{
    
    self.time = 0;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countTime) userInfo:nil repeats:YES];

    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFormatter setCurrencyCode:@"CZK"];
    //    [currencyFormatter setCurrencyCode:@"CZK"];
    [currencyFormatter setMaximumFractionDigits:2];
    [currencyFormatter setMinimumFractionDigits:2];
    
    [currencyFormatter setGroupingSize:3];
    [currencyFormatter setGroupingSeparator:@" "];

    
    [[MLSession currentSession] updateBalanceWithBlock:^(BOOL succeeded, NSError *error) {
        
        if (succeeded) {
                self.balanceLabel.text = [NSString stringWithFormat:@"%@",[currencyFormatter stringFromNumber:@([MLSession currentSession].currentAccount.balance)]];
        }
        
  
        
    }];
    
    if ([MLSession currentSession].currentAccount.isPetrol) {
        [[MLSession currentSession] getPetrolUsersWithBlock:^(BOOL succeeded, NSError *error) {
            
        }];
    }
    
    
    [MLTransaction fetchAllWithBlock:^(BOOL succeeded, NSError *error) {
        
        if (succeeded) {
            [self trackUpdate];
            self.results = [[MLTransaction allObjects] sortedResultsUsingProperty:@"date" ascending:NO];
            
            if (self.results.count>0) {
                self.emptyLabel.hidden = YES;

            }
            else{
                self.emptyLabel.hidden = NO;

            }
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"chrono"] == YES) {
                [self.tableView reloadData];
                
                
            }
            else{
                
                if (self.sections.count>0) {
                    [self.sections removeAllObjects];
                }
                
                RLMResults*categories = [MLCategory allObjects];
                
                for (MLCategory *category in categories) {
                    
                    MLSection*section = [[MLSection alloc] init];
                    section.category = category;
                    
                    NSMutableArray*sectiontrans = [NSMutableArray array];
                    
                    RLMResults*transactions = [MLTransaction objectsWhere:@"category = %@",category.categoryId ];
                    
                    for (MLTransaction*trans in transactions) {
                        [sectiontrans addObject:trans];
                    }
                    
                    section.transactions = [self dateSortArray:sectiontrans];
                    
                    if (section.transactions.count>0) {
                        [self.sections addObject:section];
                    }
                    
                }
                
                NSMutableArray*sectionToSort = self.sections;
                
                self.sections = [self nameSortArray:sectionToSort];
                
                [self.tableView reloadData];
                
                
            }
            
        }
        
    }];
    
    [[MLSession currentSession] updateAccountWithBlock:^(BOOL succeeded, NSError *error) {
        
    }];

    
 
}




-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"transaction_list_screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateBalance)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateBalance)
                                                 name:@"updateTransactionsAndBalance"
                                               object:nil];


    [FXReachability sharedInstance];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateStatus) name:FXReachabilityStatusDidChangeNotification object:nil];
    


    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.results = [[MLTransaction allObjects] sortedResultsUsingProperty:@"date" ascending:NO];
    
    if (self.results.count>0) {
        self.emptyLabel.hidden = YES;
        
    }
    else{
        self.emptyLabel.hidden = NO;
        
    }
    
    
    if ([MLSession currentSession].currentAccount.isPetrol) {
        self.navigationItem.leftBarButtonItem.title = [MLSession currentSession].currentAccount.user.fullName;
        self.addButton.hidden = YES;
        
       


       
    }
    else{
        self.navigationItem.leftBarButtonItem = nil;
        self.addButton.hidden = NO;

    }
    
   
    

    
    [self updateBalance];

}


-(NSMutableArray*)dateSortArray:(NSMutableArray *)toBeSorted
{
    NSArray *sortedArray;
    sortedArray = [toBeSorted sortedArrayUsingComparator:^NSComparisonResult(MLTransaction* a, MLTransaction* b)
                   {
                       return [b.date compare:a.date];
                   }];
    return [sortedArray mutableCopy];
}

-(NSMutableArray*)nameSortArray:(NSMutableArray *)toBeSorted
{
    NSArray *sortedArray;
    sortedArray = [toBeSorted sortedArrayUsingComparator:^NSComparisonResult(MLSection* a, MLSection* b)
                   {
                       return [a.category.name compare:b.category.name];
                   }];
    return [sortedArray mutableCopy];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)countTime{

    self.time++;

}

-(void)refreshTransactions{
    
    self.time = 0;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countTime) userInfo:nil repeats:YES];
    
    if (self.refreshControl.isRefreshing) {
        [self.refreshControl endRefreshing];
    }
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

    [SVProgressHUD showWithStatus:@"Aktualizuji transakce"];
   
    
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFormatter setCurrencyCode:@"CZK"];
    //    [currencyFormatter setCurrencyCode:@"CZK"];
    [currencyFormatter setMaximumFractionDigits:2];
    [currencyFormatter setMinimumFractionDigits:2];
    
    [currencyFormatter setGroupingSize:3];
    [currencyFormatter setGroupingSeparator:@" "];
    
    
    
    [[MLSession currentSession] updateBalanceWithBlock:^(BOOL succeeded, NSError *error) {
        
        if (succeeded) {
            self.balanceLabel.text = [NSString stringWithFormat:@"%@",[currencyFormatter stringFromNumber:@([MLSession currentSession].currentAccount.balance)]];
        }
        
        
        
    }];
    
    
    [MLTransaction fetchAllWithBlock:^(BOOL succeeded, NSError *error) {
        [SVProgressHUD dismiss];
        if (succeeded) {
            self.results = [[MLTransaction allObjects] sortedResultsUsingProperty:@"date" ascending:NO];
            [self trackRefresh];
            if (self.results.count>0) {
                self.emptyLabel.hidden = YES;
                
            }
            else{
                self.emptyLabel.hidden = NO;
                
            }
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"chrono"] == YES) {
                [self.tableView reloadData];
                
                
            }
            else{
                
                if (self.sections.count>0) {
                    [self.sections removeAllObjects];
                }
                
                RLMResults*categories = [MLCategory allObjects];
                
                for (MLCategory *category in categories) {
                    
                    MLSection*section = [[MLSection alloc] init];
                    section.category = category;
                    
                    NSMutableArray*sectiontrans = [NSMutableArray array];
                    
                    RLMResults*transactions = [MLTransaction objectsWhere:@"category = %@",category.categoryId ];
                    
                    for (MLTransaction*trans in transactions) {
                        [sectiontrans addObject:trans];
                    }
                    
                    section.transactions = [self dateSortArray:sectiontrans];
                    
                    if (section.transactions.count>0) {
                        [self.sections addObject:section];
                    }
                    
                }
                
                NSMutableArray*sectionToSort = self.sections;
                
                self.sections = [self nameSortArray:sectionToSort];
                
                [self.tableView reloadData];
                
                
            }
            
        }
        
    }];


    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"chrono"] == YES) {
        return 0;
    }
    else{
        return 36;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"chrono"] == YES) {
        return nil;
    }
    
    UILabel*label = [UILabel new];
    label.font = [UIFont mediumFontOfSize:14];
    label.textColor = [UIColor colorFromHexCode:@"#ffffff" andAlpha:1.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor colorFromHexCode:@"#333444" andAlpha:0.85];
    label.layer.cornerRadius = 18;
    label.clipsToBounds = YES;
    MLSection*sectionSel = self.sections[section];
    MLCategory*category = sectionSel.category;
    
    label.text = category.name;
    
    [label sizeToFit];
    
    label.frame = CGRectMake(15, 5, label.frame.size.width+20, 36);
    
    UIView*view = [UIView new];
    view.frame = CGRectMake(0, 0, self.view.frame.size.width, 36);
    view.backgroundColor = [UIColor clearColor];
    [view addSubview:label];

    return view;

}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"chrono"] == YES) {
        return nil;
    }
    
    MLSection*sectionSel = self.sections[section];
    MLCategory*category = sectionSel.category;
    return category.name;

}

- (void)passcodeViewController:(BKPasscodeViewController *)aViewController didFinishWithPasscode:(NSString *)aPasscode
{
    switch (aViewController.type) {
        case BKPasscodeViewControllerNewPasscodeType:
        case BKPasscodeViewControllerChangePasscodeType:
         
            [[NSUserDefaults standardUserDefaults]setObject:aPasscode forKey:@"passcode"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"shouldShowLockscreen"];
            [[NSUserDefaults standardUserDefaults] synchronize];
         
            break;
        default:
            break;
    }
    
    [aViewController dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"chrono"] == YES) {
        return 1;
  
    }
    else{
        return self.sections.count;
    }
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    MLTransactionTableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:@"transCell"];
    
    
 if ([[NSUserDefaults standardUserDefaults] boolForKey:@"chrono"] == YES) {
     [cell setupWithTransaction:self.results[indexPath.row]];

 }
 else{
     [cell setupWithTransaction:[self.sections[indexPath.section] transactions][indexPath.row]];

 }
  
    
    
    return cell;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
     if ([[NSUserDefaults standardUserDefaults] boolForKey:@"chrono"] == YES) {
         if (self.results) {
             return self.results.count;
             
         }
         else{
             return 0;
         }
     }
     else{
     
         if (self.results) {
             return [[self.sections[section] transactions] count];
             
         }
         else{
             return 0;
         }
     
     }
    
  
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 15)];
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    // Force your tableview margins (this may be a bad idea)
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 15)];
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{

    
         
         if ([identifier isEqualToString:@"transDetailSegue"]) {
             
            
         
         }
         else{
         
             if (![FXReachability isReachable]){
                 return NO;
             }
             
         }
    
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if ([segue.identifier isEqualToString:@"transDetailSegue"]) {
        
        MLTransactionDetailViewController*controller = (MLTransactionDetailViewController*)segue.destinationViewController;
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"chrono"] == YES) {
            controller.transaction = self.results[[self.tableView indexPathForSelectedRow].row];
        }
        else{
            
            MLSection*section = self.sections[[self.tableView indexPathForSelectedRow].section];
            
            controller.transaction = section.transactions[[self.tableView indexPathForSelectedRow].row];

        }
        
        
    }

}

-(IBAction)cardButtonTapped:(id)sender{
    
    
    
    NSMutableArray*cardStrings = [[NSMutableArray alloc] init];
    
    for (NSDictionary*dict in [MLSession currentSession].currentAccount.petrolUsers) {
        [cardStrings addObject:[NSString stringWithFormat:@"%@ %@",[[dict objectForKey:@"user"] objectForKey:@"firstName"],[[dict objectForKey:@"user"] objectForKey:@"lastName"]]];
    }
    
    [[[ActionSheetStringPicker alloc] initWithTitle:@"Karty" rows:cardStrings initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {

        NSDictionary*selectedCard= [MLSession currentSession].currentAccount.petrolUsers[selectedIndex];
        
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

        [SVProgressHUD showWithStatus:@"Probíhá změna karty" ];
        
        __block MLSession*session = [[MLSession alloc] init];
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
        
      
               [[MLSession currentSession] destroy];

               
               session.password = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentPassword"];
               session.login = [[selectedCard objectForKey:@"user"] objectForKey:@"email"];
               
               [session loginWithBlock:^(BOOL succeeded, NSError *error) {
                   
                   if (succeeded) {
                       
                       
                       
                       session = [MLSession currentSession];
                       
                       [session updateAccountWithBlock:^(BOOL succeeded, NSError *error) {
                           
                           if (succeeded) {
                               
                               [MLCostCenter fetchAllWithBlock:^(BOOL succeeded, NSError *error) {
                                   
                                   
                                   if (!succeeded) {
                                       [SVProgressHUD dismiss];
                                       
                                   }
                                   
                                   [MLCurrency fetchAllWithBlock:^(BOOL succeeded, NSError *error) {
                                       
                                       
                                       if (!succeeded) {
                                           [SVProgressHUD dismiss];
                                           
                                       }
                                       
                                       [MLCategory fetchAllWithBlock:^(BOOL succeeded, NSError *error) {
                                           
                                           
                                           if (!succeeded) {
                                               [SVProgressHUD dismiss];
                                               
                                           }
                                           
                                           [MLTransaction fetchAllWithBlock:^(BOOL succeeded, NSError *error) {
                                               
                                               if (!succeeded) {
                                                   [SVProgressHUD dismiss];

                                               }
                                               
                                               if (session.currentAccount.isPetrol) {
                                                   
                                                   [session getPetrolUsersWithBlock:^(BOOL succeeded, NSError *error) {
                                                       [SVProgressHUD dismiss];
                                                          self.navigationItem.leftBarButtonItem.title = [MLSession currentSession].currentAccount.user.fullName;
                                
                                                       [self updateBalance];
                                                       
                                                       
                                                   }];
                                                   
                                               }
                                               
                                           }];
                                           
                                           
                                           
                                           
                                       }];
                                       
                                       
                                       
                                   }];
                                   
                                   
                                   
                               }];
                               
                               
                               
                               
                               
                               
                               
                               
                               
                               
                           }
                           else{
                               [SVProgressHUD dismiss];

                           }
                           
                       }];
                       
                       
                       
                   }
                   else{
                       [SVProgressHUD dismiss];   
                   }
                   
               }];
               
          
           
             
      

      });
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:sender] showActionSheetPicker];
    
    
}



-(void)registerForPush{
    
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    
}

-(void)updateStatus{
    
    if ([FXReachability isReachable]) {
        
        if ([JDStatusBarNotification isVisible]) {
            [JDStatusBarNotification dismiss];

        }
        
    }
    else{
        
        if (![JDStatusBarNotification isVisible]) {
              [JDStatusBarNotification showWithStatus:@"Offline režim" styleName:JDStatusBarStyleError];
        }
        
      

    }
    
     
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent; // your own style
}

-(void)trackRefresh{
    
    [self.timer invalidate];
    self.timer = nil;
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"transactions"                      // Timing category (required)
                                                         interval:@((NSUInteger)(self.time * 1000))   // Timing interval (required)
                                                             name:@"refresh"                     // Timing name
                                                            label:nil] build]];
    self.time = 0;

}

-(void)trackUpdate{
    
    [self.timer invalidate];
    self.timer = nil;
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"transactions"                      // Timing category (required)
                                                         interval:@((NSUInteger)(self.time * 1000))   // Timing interval (required)
                                                             name:@"refresh_update"                     // Timing name
                                                            label:nil] build]];
    self.time = 0;
}

@end
