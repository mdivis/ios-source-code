//
//  UIFont+UGFonts.m
//  Pincamp
//
//  Created by Pavel Nemecek on 8/25/13.
//  Copyright (c) 2013 Unigram. All rights reserved.
//

#import "UIFont+MOFonts.h"

@implementation UIFont (MOFonts)


+ (UIFont *)regularFontOfSize:(CGFloat)size {

    return [UIFont fontWithName:@"Avenir-Roman" size:size];
}

+ (UIFont *)regularTextFontOfSize:(CGFloat)size {
    
    return [UIFont fontWithName:@"Aveni" size:size];
}

+ (UIFont *)boldTextFontOfSize:(CGFloat)size {
    
    return [UIFont fontWithName:@"Merriweather-Bold" size:size];
}

+ (UIFont *)mediumFontOfSize:(CGFloat)size {
    
   return [UIFont fontWithName:@"Avenir-Medium" size:size];}



+ (UIFont *)boldFontOfSize:(CGFloat)size {

    return [UIFont fontWithName:@"AvenirNext-Bold" size:size];

}



@end
