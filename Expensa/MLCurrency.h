//
//  MLCurrency.h
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//
#import <Foundation/Foundation.h>

#import <Realm/Realm.h>
#import "RLMObject+JSON.h"
#import "MLApiManager.h"
#import "Constants.h"
#import "MLSession.h"

@interface MLCurrency : RLMObject
@property NSString*currencyId;
@property NSString*symbol;
//@property NSString*name;

+(void)fetchAllWithBlock:(void (^)(BOOL succeeded, NSError *error))block;

+(instancetype)currencyFromDictionary:(NSDictionary*)dictionary;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<MLCurrency>
RLM_ARRAY_TYPE(MLCurrency)
