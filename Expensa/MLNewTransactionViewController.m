//
//  MLNewTransactionViewController.m
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import "MLNewTransactionViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MLCategory.h"
#import "MLCurrency.h"
#import "MLCostCenter.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "SVProgressHUD.h"
#import "AbstractActionSheetPicker.h"
#import "ActionSheetStringPicker.h"
#import "ActionSheetDatePicker.h"
#import "MLTransaction.h"
#import "UIImage+ResizeMagick.h"
#import "BKCurrencyTextField.h"

#import "MOImagePreviewViewController.h"
#import <Google/Analytics.h>

@interface MLNewTransactionViewController ()<UITextViewDelegate, UITextFieldDelegate,UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate>

@property(nonatomic,weak) IBOutlet UIScrollView*scrollView;
@property(nonatomic,weak) IBOutlet BKCurrencyTextField*pyedField;
@property(nonatomic,weak) IBOutlet UITextField*placeField;
@property(nonatomic,weak) IBOutlet BKCurrencyTextField*personalField;
@property(nonatomic,weak) IBOutlet UITextView*noteView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photoHeight;
@property(nonatomic,weak) IBOutlet UIButton*categoryButton;
@property(nonatomic,weak) IBOutlet UIButton*currencyButton;
@property(nonatomic,weak) IBOutlet UIButton*dateButton;
@property(nonatomic,weak) IBOutlet UIButton*expenseCenterButton1;
@property(nonatomic,weak) IBOutlet UIButton*expenseCenterButton2;

@property(nonatomic,weak) IBOutlet UIButton*photoButton;
@property(nonatomic,weak) IBOutlet UIImageView*photoView;



@property (nonatomic,strong) MLCurrency*activeCurrency;
@property (nonatomic,strong) MLCostCenter*activeCostCenter1;
@property (nonatomic,strong) MLCostCenter*activeCostCenter2;
@property (nonatomic,strong) NSDate*activeDate;
@property (nonatomic,strong) MLCategory *activeCategory;

@property (nonatomic,strong) RLMResults *primaryCenters;
@property (nonatomic,strong) RLMResults *secondaryCenters;
@property (nonatomic,strong) RLMResults *currencies;
@property (nonatomic,strong) RLMResults *categories;
@property (nonatomic,strong) RLMResults *selectCategories;

@property (nonatomic,strong) UIImage *selectedImage;
@property(nonatomic,weak) IBOutlet UIButton*detailButton;

@property(nonatomic,strong) NSTimer *timer;
@property (nonatomic,assign) NSInteger time;


@end

@implementation MLNewTransactionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.time = 0;
    
    self.primaryCenters = [MLCostCenter objectsWhere:@"isPrimary = 1"];
    self.secondaryCenters = [MLCostCenter objectsWhere:@"isSecondary = 1"];
    self.categories = [MLCategory objectsWhere:@"categoryId != 'atm'"];
    self.currencies = [[MLCurrency allObjects] sortedResultsUsingProperty:@"symbol" ascending:YES];
    
    self.photoButton.layer.cornerRadius = 18.0;

    [self.pyedField.numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [self.pyedField.numberFormatter setMinimumFractionDigits:2];
    [self.pyedField.numberFormatter setMaximumFractionDigits:2];

    
    [self.personalField.numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [self.personalField.numberFormatter setMinimumFractionDigits:2];
    [self.personalField.numberFormatter setMaximumFractionDigits:2];
    
    NSDateFormatter*formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd. MM. yyyy";
    
    [self.dateButton setTitle: [formatter stringFromDate:[NSDate date]] forState:UIControlStateNormal];
    
    NSString*currency = nil;
    MLCurrency*selectedCurrency;
    RLMResults*results = [MLCurrency objectsWhere:@"currencyId = %@",@"czk"];
    
    
    if (results.count>0) {
        selectedCurrency = results[0];
        
    }
    
    
    self.activeDate = [NSDate date];
    self.activeCurrency = selectedCurrency;
    
    
    currency = selectedCurrency.symbol;
    
    [self.currencyButton setTitle:currency forState:UIControlStateNormal];

    
    if (self.primaryCenters.count<1 ){
        self.expenseCenterButton1.enabled = NO;
    }
    if (self.secondaryCenters.count<1 ){
        self.expenseCenterButton2.enabled = NO;
    }

    self.detailButton.hidden = YES;
    
    self.photoHeight.constant = 0;


}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"transaction_new_screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
  }


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)dismiss:(id)sender{

    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(IBAction)categoryButtonTapped:(id)sender{
    
    [self.view endEditing:YES];
    
    NSMutableArray*categoriesStrings = [[NSMutableArray alloc] init];
    
    for (MLCategory *category in self.categories) {
        
       
            [categoriesStrings addObject:category.name];

     
        
    }
    
    [[[ActionSheetStringPicker alloc] initWithTitle:@"Kategorie" rows:categoriesStrings initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        self.activeCategory = self.categories[selectedIndex];
        [self.categoryButton setTitle:selectedValue forState:UIControlStateNormal];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:sender] showActionSheetPicker];
    
    
    
    
}
-(IBAction)currencyButtonTapped:(id)sender{
    [self.view endEditing:YES];

    
    NSMutableArray*categoriesStrings = [[NSMutableArray alloc] init];
    
    for (MLCurrency *category in self.currencies) {
        [categoriesStrings addObject:category.symbol];
    }
    
    int selectedIndex=0;
    
    for (int i =0; i<self.currencies.count; i++) {
        if ([[self.currencies[i] currencyId] isEqualToString:self.activeCurrency.currencyId]) {
            selectedIndex = i;
            break;
        }
    }
    
    [[[ActionSheetStringPicker alloc] initWithTitle:@"Měna" rows:categoriesStrings initialSelection:selectedIndex doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        self.activeCurrency = self.currencies[selectedIndex];
        [self.currencyButton setTitle:self.activeCurrency.symbol forState:UIControlStateNormal];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:sender] showActionSheetPicker];
    
    
}
-(IBAction)dateButtonTapped:(id)sender{
    [self.view endEditing:YES];

    ActionSheetDatePicker *picker =
    
    [[ActionSheetDatePicker alloc] initWithTitle:@"Datum" datePickerMode:UIDatePickerModeDate selectedDate:self.activeDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        self.activeDate=selectedDate;
        NSDateFormatter*formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd. MM. yyyy";
        
        [self.dateButton setTitle: [formatter stringFromDate:selectedDate] forState:UIControlStateNormal];
        
        
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:sender];
    
    picker.maximumDate = [NSDate date];
    [picker showActionSheetPicker];
}
-(IBAction)expenseCenterButton2Tapped:(id)sender{
    [self.view endEditing:YES];

    
    NSMutableArray*categoriesStrings = [[NSMutableArray alloc] init];
    
    for (MLCostCenter *category in self.secondaryCenters) {
        [categoriesStrings addObject:category.name];
    }
    
    [[[ActionSheetStringPicker alloc] initWithTitle:@"Nákladové centrum 2" rows:categoriesStrings initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        self.activeCostCenter2 = self.secondaryCenters[selectedIndex];
        [self.expenseCenterButton2 setTitle:selectedValue forState:UIControlStateNormal];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:sender] showActionSheetPicker];
    
    
}
-(IBAction)expenseCenterButton1Tapped:(id)sender{
    
    [self.view endEditing:YES];

    
    NSMutableArray*categoriesStrings = [[NSMutableArray alloc] init];
    
    for (MLCostCenter *category in self.primaryCenters) {
        [categoriesStrings addObject:category.name];
    }
    
    [[[ActionSheetStringPicker alloc] initWithTitle:@"Nákladové centrum 1" rows:categoriesStrings initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        self.activeCostCenter1 = self.primaryCenters[selectedIndex];
        [self.expenseCenterButton1 setTitle:selectedValue forState:UIControlStateNormal];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:sender] showActionSheetPicker];
    
    
}



-(IBAction)saveTransaction:(id)sender{

    if ([[self.pyedField numberValue] doubleValue]<[[self.personalField numberValue] doubleValue]) {
        [[[UIAlertView alloc] initWithTitle:@"Osobní útrata nemůže být vyšší než výše samotné platby." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return;
    }

    if (self.placeField.text.length<1 || [self.pyedField.numberValue doubleValue]==0) {
        [[[UIAlertView alloc] initWithTitle:@"Zadejte údaje" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return;
    }
    
    if (!self.activeCategory) {
        [[[UIAlertView alloc] initWithTitle:@"Vyberte kategorii" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return;
    }
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user_action"     // Event category (required)
                                                          action:@"saved_new_transaction"  // Event action (required)
                                                           label:nil          // Event label
                                                           value:nil] build]];    // Event value
    

    [self.view endEditing:YES];
    
    RLMRealm*realm=  [RLMRealm defaultRealm];
    
    MLTransaction*transaction = [[MLTransaction alloc] init];
    
    
    transaction.currency = self.activeCurrency.currencyId;
    
    if (self.placeField.text.length>0) {
        transaction.merchant = self.placeField.text;
    }
    
    if (self.noteView.text.length>0) {
        transaction.note = self.noteView.text;
    }
    else{
    transaction.note = @"";
    }
    if ([self.pyedField.numberValue doubleValue]>0) {
        transaction.priceWithVat = [self.pyedField.numberValue doubleValue];
    }
    if ([self.personalField.numberValue doubleValue]>0) {
        transaction.privateSpendingAmount = [self.personalField.numberValue doubleValue];
    }
    else{
        transaction.privateSpendingAmount = 0;
    }
    transaction.date = self.activeDate;
    
    if (self.activeCostCenter1) {
        transaction.primaryCostCenterId = self.activeCostCenter1.costCenterId;
    }
    else{
        transaction.primaryCostCenterId = 0;
    }
    if (self.activeCostCenter2) {
        transaction.secondaryCostCenterId = self.activeCostCenter2.costCenterId;
    }
    else{
        transaction.secondaryCostCenterId = 0;
    }
    transaction.category = self.activeCategory.categoryId;
    
    transaction.isManual = YES;
    transaction.fxDifference = 0;
    transaction.state = @"";
    transaction.originalCurrency = self.activeCurrency.currencyId;
    transaction.originalPrice = [self.pyedField.text doubleValue];
    transaction.isEditable = YES;
    transaction.transactionId = 0;
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

    [SVProgressHUD showWithStatus:@"Ukládám data" ];
    
    [transaction createInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [SVProgressHUD dismiss];
        
        if (succeeded) {
            
            [realm beginWriteTransaction];
            [realm addObject:transaction];
            [realm commitWriteTransaction];
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

            [SVProgressHUD showInfoWithStatus:@"Transakce uložena"];
            
            if (self.selectedImage) {
                [self updateImage:transaction];
            }
            else{
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            
        }
        else{
            [[[UIAlertView alloc] initWithTitle:@"Při ukládání došlo k chybě." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
        
    }];
    
    
}

-(IBAction)photoButtonTapped:(id)sender{
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: @"Zrušit"
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: @"Vyfotit", @"Vybrat z uložených", nil];
    [actionSheet showInView:self.view];
    
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self takeNewPhotoFromCamera];
            break;
        case 1:
            [self choosePhotoFromExistingImages];
        default:
            break;
    }
}

- (void)takeNewPhotoFromCamera
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.allowsEditing = NO;
        controller.mediaTypes =  [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
        controller.delegate = self;
        [self.navigationController presentViewController: controller animated: YES completion: nil];
    }
}

-(void)choosePhotoFromExistingImages
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        controller.allowsEditing = NO;
        controller.mediaTypes =  [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
        controller.delegate = self;
        
        [self.navigationController presentViewController: controller animated: YES completion: nil];
    }
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
    
    
    
    
    UIImage *originalImage, *editedImage, *imageToSave;
    
    editedImage = (UIImage *) [info objectForKey:
                               
                               UIImagePickerControllerEditedImage];
    
    originalImage = (UIImage *) [info objectForKey:
                                 
                                 UIImagePickerControllerOriginalImage];
    
    
    
    if (editedImage) {
        
        imageToSave = editedImage;
        
    } else {
        
        imageToSave = originalImage;
        
    }
    
    
    UIImage* resizedImage = [imageToSave resizedImageByMagick: @"800x800"];

    UIImage*scaledImage = [imageToSave resizedImageByWidth:self.view.frame.size.width];
    self.photoHeight.constant = scaledImage.size.height;
    self.photoView.image = scaledImage;

    self.selectedImage = resizedImage;
    
    if (self.selectedImage) {
        self.detailButton.hidden = NO;
    }
    else{
        self.detailButton.hidden = YES;
        self.photoHeight.constant = 0;


    }
    
    
}



- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
{
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
}

-(void)updateImage:(MLTransaction*)transaction{
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

    [SVProgressHUD showWithStatus:@"Ukládám foto"];
    self.time = 0;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countTime) userInfo:nil repeats:YES];
    
    
    NSData * data = UIImageJPEGRepresentation(self.selectedImage,0.8) ;
    
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@single-user/transactions/%ld/photo", ApiBaseUrl, (long)transaction.transactionId] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:data name:@"file" fileName:@"soubor.jpg" mimeType:@"image/jpeg"];
        [formData appendPartWithFormData:[@"soubor" dataUsingEncoding:NSUTF8StringEncoding] name:@"note"];
    } error:nil];
    
    
    
    NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    
    [request setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];
    
    
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSProgress *progress = nil;
    
    NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithStreamedRequest:request progress:&progress completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        [SVProgressHUD dismiss];
        if (error) {
            [self.timer invalidate];
            self.timer = nil;
            NSLog(@"Error: %@", error);
           [[[UIAlertView alloc] initWithTitle:@"Při ukládání došlo k chybě." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        } else {
            NSLog(@"%@ %@", response, responseObject);
            [self trackUpdate];
            
            RLMRealm*realm = [RLMRealm defaultRealm];
            
            MLTransactionPhoto*photo = [[MLTransactionPhoto alloc] init];
            photo.transactionId = transaction.transactionId;
            photo.note = @"soubor";
            photo.warning = NO;
            photo.fileData = data;
            
            
            [realm beginWriteTransaction];
            [realm addObject:photo];
            transaction.photo = photo;
            [realm commitWriteTransaction];
            
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

            [SVProgressHUD showInfoWithStatus:@"Fotografie uložena" ];
            
            [self dismissViewControllerAnimated:YES completion:nil];

            
            
        }
    }];
    
    [uploadTask resume];
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == self.placeField) {
        [self.pyedField becomeFirstResponder];
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField==self.pyedField  || textField==self.personalField) {
        
        UITextPosition *beginning = [textField endOfDocument];
        [textField setSelectedTextRange:[textField textRangeFromPosition:beginning
                                                              toPosition:beginning]];
        
        UIToolbar *doneToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        doneToolbar.barStyle = UIBarStyleBlackTranslucent;
        
        
        UIBarButtonItem*doneItem =  [[UIBarButtonItem alloc] initWithTitle:@"Hotovo" style:UIBarButtonItemStyleBordered target:self action:@selector(hideInputView)];
        
        
        doneToolbar.items = [NSArray arrayWithObjects:
                             
                             [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                             doneItem,
                             nil];
        
        
        [doneToolbar sizeToFit];
        
        textField.inputAccessoryView = doneToolbar;
        
    }
}

- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return NO;
    }else{
        return YES;
    }
}




-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}

-(void)keyboardWillShow:(NSNotification *)notification
{
    // self.scrollView.contentSize needs to be set appropriately
    
    if ([self.noteView isFirstResponder]) {
        
        
        
        CGRect keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        
        CGRect visibleArea = self.view.frame;
        visibleArea.size.height -= keyboardRect.size.height;
        CGPoint scrollPoint = CGPointZero;
        
        CGPoint visiblePoint = CGPointMake(0, self.noteView.frame.origin.y + self.noteView.frame.size.height + 70);
        
        if (!CGRectContainsPoint(visibleArea, visiblePoint)) {
            scrollPoint = CGPointMake(0.0, visiblePoint.y - keyboardRect.origin.y);
        }
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, scrollPoint.y, 0.0);
        
        // on iOS 7, the keyboard uses new, undocumented animation curve with integer value of 7.
        // Which is why we can't use block animations here
        // http://stackoverflow.com/questions/18837166/how-to-mimic-keyboard-animation-on-ios-7-to-add-done-button-to-numeric-keyboar
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
        [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        self.scrollView.contentInset = contentInsets;
        self.scrollView.scrollIndicatorInsets = contentInsets;
        self.scrollView.contentOffset = scrollPoint;
        
        [UIView commitAnimations];
    }
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.scrollView.scrollIndicatorInsets = self.scrollView.contentInset;
    
    [UIView commitAnimations];
}

-(IBAction)detailButtonTapped:(id)sender{
    
    MOImagePreviewViewController*controller =[[MOImagePreviewViewController alloc] initWithNibName:@"MOImagePreviewViewController" bundle:nil imageDate: UIImageJPEGRepresentation(self.photoView.image,1)];
    
    UINavigationController*nav = [[UINavigationController alloc] initWithRootViewController:controller];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
    
}




-(void)hideInputView{
    [self.view endEditing:YES];
}


-(void)trackUpdate{
    
    [self.timer invalidate];
    self.timer = nil;
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"photos"                      // Timing category (required)
                                                         interval:@((NSUInteger)(self.time * 1000))   // Timing interval (required)
                                                             name:@"upload"                     // Timing name
                                                            label:nil] build]];
    self.time = 0;
}

-(void)countTime{
    
    self.time++;
    
}

@end
