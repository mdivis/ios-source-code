//
//  UIColor+UGColors.m
//  Pincamp
//
//  Created by Pavel Nemecek on 8/25/13.
//  Copyright (c) 2013 Unigram. All rights reserved.
//

#import "UIColor+MOColors.h"

@implementation UIColor (MOColors)

+ (UIColor *) colorFromHexCode:(NSString *)hexString {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

+ (UIColor *) colorFromHexCode:(NSString *)hexString andAlpha:(float)alphaValue {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alphaValue];
}

+ (UIColor *) navBarTextColor {
    
    static UIColor * color;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color =  [UIColor colorFromHexCode:@"#858585" andAlpha:1.0f];

    });
    return color;
    
     
}

+ (UIColor *) navBarBackgroundColor{
    static UIColor * color;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color =  [UIColor colorFromHexCode:@"#ffffff" andAlpha:0.90f];
        
    });
    return color;

}

+ (UIColor *) georgiaTextColor {
    
    static UIColor * color;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color =  [UIColor colorFromHexCode:@"#656565" andAlpha:1.0f];
        
    });
    return color;
    
    
}

+ (UIColor *) headerTextColor {
    
    static UIColor * color;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color =  [UIColor colorFromHexCode:@"#000000" andAlpha:1.0f];
        
    });
    return color;
    
    
}



+ (UIColor *) separatorColor {
    
    static UIColor * color;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color =  [UIColor colorFromHexCode:@"#c8c7cc" andAlpha:1.0f];
        
    });
    return color;
    
    
}

+ (UIColor *) subHeaderTextColor {
    
    static UIColor * color;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color =  [UIColor colorFromHexCode:@"#858585" andAlpha:1.0f];
        
    });
    return color;
    
    
}

+ (UIColor *) buttonColor {
    
    static UIColor * color;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color =  [UIColor colorFromHexCode:@"#000000" andAlpha:1.0f];
        
    });
    return color;
    
    
}

+ (UIColor *) buttonHighlightColor {
    
    static UIColor * color;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color =  [UIColor colorFromHexCode:@"#ffffff" andAlpha:1.0f];
        
    });
    return color;
    
    
}

+ (UIColor *) navBarButtonColor {
    
    static UIColor * color;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color =  [UIColor colorFromHexCode:@"#ffffff" andAlpha:1.0f];
        
    });
    return color;
    
    
}

+ (UIColor *) navBarButtonHighlightColor {
    
    static UIColor * color;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color =  [UIColor colorFromHexCode:@"#000000" andAlpha:1.0f];
        
    });
    return color;
    
    
}





@end
