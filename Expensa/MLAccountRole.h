//
//  AccountRole.h
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MLAccountRole : NSObject

@property (nonatomic,strong) NSString*name;

+(instancetype)roleWithName:(NSString*)name;

@end
