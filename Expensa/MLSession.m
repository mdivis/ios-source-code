//
//  MOSession.m
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import "MLSession.h"
#import "NSDictionary+Value.h"
#import "MLAccount.h"
#import "MLApiManager.h"
#import "Constants.h"



@implementation MLSession

+ (instancetype)currentSession {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"currentUserKey"];
    MLSession *session = nil;
    if (data) {
        session = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
    }
    return session;
}

-(void)destroy{
    
    NSUserDefaults*defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"currentUserKey"];
    [defaults synchronize];
    
}

-(void)update{
    [self archive:self withKey:@"currentUserKey"];
}

- (BOOL)archive:(MLSession *)session withKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = nil;
    if (session) {
        data = [NSKeyedArchiver archivedDataWithRootObject:session];
    }
    [defaults setObject:data forKey:key];
    return [defaults synchronize];
}


+(instancetype)sessionFromDictionary:(NSDictionary*)dictionary{

    MLSession*session = [[MLSession alloc] init];
    session.token = [dictionary stringValueForKey:@"token"];
    session.login = [dictionary stringValueForKey:@"login"];
    session.validUntil = [dictionary dateValueForKey:@"validUntil"];
    session.deviceId = [dictionary stringValueForKey:@"appInstanceId"];

    NSMutableArray*array = [NSMutableArray array];
    for (NSDictionary*dict in [dictionary objectForKey:@"accounts"]) {
        [array addObject:[MLAccount accountFromDisctionry:dict]];
    }
    session.accounts = [array copy];
    session.currentAccount = session.accounts[0];
    
    return session;

}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.token forKey:@"token"];
    [aCoder encodeObject:self.deviceId forKey:@"deviceId"];
    [aCoder encodeObject:self.login forKey:@"login"];
    [aCoder encodeObject:self.validUntil forKey:@"validUntil"];
    [aCoder encodeObject:self.accounts forKey:@"accounts"];
    [aCoder encodeObject:self.currentAccount forKey:@"currentAccount"];

}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if((self = [super init])) {
        self.token =  [aDecoder decodeObjectForKey:@"token"];
        self.login = [aDecoder decodeObjectForKey:@"login"];
        self.deviceId = [aDecoder decodeObjectForKey:@"deviceId"];
        self.validUntil = [aDecoder decodeObjectForKey:@"validUntil"];
        self.accounts = [aDecoder decodeObjectForKey:@"accounts"];
        self.currentAccount = [aDecoder decodeObjectForKey:@"currentAccount"];

    }
    return self;
}

-(void)loginWithBlock:(void (^)(BOOL succeeded, NSError *error))block{
    
    NSDictionary*parameters = nil;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"lastToken"]) {
    parameters = @{@"login":self.login,@"password":self.password, @"appInstanceId":[[NSProcessInfo processInfo] globallyUniqueString], @"platform":@"APNS", @"notificationDeviceId":[[NSUserDefaults standardUserDefaults] objectForKey:@"lastToken"]};
    }
    else{
         parameters = @{@"login":self.login,@"password":self.password, @"appInstanceId":[[NSProcessInfo processInfo] globallyUniqueString]};
    }
    
  
    [MLApiManager sharedInstace].requestSerializer = [AFHTTPRequestSerializer serializer];
    [[MLApiManager sharedInstace] POST:ApiUserLogin parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {

        MLSession*session = [MLSession sessionFromDictionary:responseObject];
        
        block([self archive:session withKey:@"currentUserKey"], nil);
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"LOGIN ERROR %@",error.description);
        block(NO, error);
        
    }];
    
    
}

-(void)updateAccountWithBlock:(void (^)(BOOL succeeded, NSError *error))block{
    
    NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    [MLApiManager sharedInstace].requestSerializer = [AFHTTPRequestSerializer serializer];
    [[[MLApiManager sharedInstace] requestSerializer] setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];

    [[MLApiManager sharedInstace] GET:ApiUser parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        

        
        self.currentAccount.teamName = [responseObject stringValueForKey:@"teamName"];
        
        if ([responseObject objectForKey:@"approveManager"] != [NSNull null]) {
            self.currentAccount.approveManager = [responseObject[@"approveManager"] stringValueForKey:@"fullName"];
            
        }
        
        
        
        if ([responseObject objectForKey:@"user"]) {
            
            self.currentAccount.user = [MLUser userFromDictionary:[responseObject objectForKey:@"user"]];
        }

        
        block([self archive:self withKey:@"currentUserKey"], nil);
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"LOGIN ERROR %@",error.description);
        block(NO, error);
        
    }];
    
    
}

-(void)updateBalanceWithBlock:(void (^)(BOOL succeeded, NSError *error))block{
    
    NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    [MLApiManager sharedInstace].requestSerializer = [AFJSONRequestSerializer serializer];
    [[[MLApiManager sharedInstace] requestSerializer] setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];
    
    [[MLApiManager sharedInstace] GET:@"single-user/card/?filter=balance" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        

        
        self.currentAccount.balance = [responseObject doubleValueForKey:@"balance" defaultValue:0];
        
        
        block([self archive:self withKey:@"currentUserKey"], nil);
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"LOGIN ERROR %@",error.description);
        block(NO, error);
        
    }];
    
    
}

-(void)currentBalanceWithBlock:(void (^)(double balance, NSError *error))block{
    
    NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    
    [MLApiManager sharedInstace].requestSerializer = [AFJSONRequestSerializer serializer];
    [[[MLApiManager sharedInstace] requestSerializer] setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];
    
    
    
    
    [[MLApiManager sharedInstace] GET:[NSString stringWithFormat:@"user/balance/%ld",(long)[MLSession currentSession].currentAccount.user.userId] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"GET BALANCE  %@",responseObject);
        
        
        block(0, nil);
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"GET BALANCE  %@",error.description);
        block(0, error);
        
    }];
    
    
}


-(void)getPetrolUsersWithBlock:(void (^)(BOOL succeeded, NSError *error))block{
    
    NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    [MLApiManager sharedInstace].requestSerializer = [AFHTTPRequestSerializer serializer];
    [[[MLApiManager sharedInstace] requestSerializer] setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];
    
    [[MLApiManager sharedInstace] GET:@"company/petrol-users/" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSArray*array = [responseObject objectForKey:@"data"];
        
        if (array.count>0) {
            self.currentAccount.petrolUsers = array;
        }
        
        block([self archive:self withKey:@"currentUserKey"], nil);
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"LOGIN ERROR %@",error.description);
        block(NO, error);
        
    }];
    
    
}



-(void)logoutWithBlock:(void (^)(BOOL succeeded, NSError *error))block{
    
    
    NSDictionary*parameters = @{@"login":self.login, @"appInstanceId":self.deviceId};

    
    [MLApiManager sharedInstace].requestSerializer = [AFHTTPRequestSerializer serializer];

    [[MLApiManager sharedInstace] POST:ApiUserLogout parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        
        block(YES, nil);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        block(NO, error);
        
    }];
    
}

- (void)updatePushTokenWithDictionary:(NSDictionary *)userDictionary block:(void (^)(BOOL succeeded, NSError *error))block{
    
    self.pushToken= userDictionary[@"notificationDeviceId"];
    
    NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    [MLApiManager sharedInstace].requestSerializer = [AFJSONRequestSerializer serializer];
    [[[MLApiManager sharedInstace] requestSerializer] setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];
    
    
    [[MLApiManager sharedInstace] PUT:@"token" parameters:userDictionary success:^(NSURLSessionDataTask *task, id responseObject) {
//        self.token = [responseObject stringValueForKey:@"token"];
        
//        block([self archive:self withKey:@"currentUserKey"], nil);

        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"LOGIN ERROR %@",error.localizedDescription);

        block(NO, error);
        
    }];
}





@end
