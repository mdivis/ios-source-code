//
//  MLLoginContoller.m
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import "MLLoginContoller.h"
#import "MLSession.h"
#import "SVProgressHUD.h"
#import "MLCostCenter.h"
#import "MLTransaction.h"
#import "MLCurrency.h"
#import "MLCategory.h"
#import "MLAppDelegate.h"
#import "UIColor+MOColors.h"
#import "RTWalkthroughPageViewController.h"
#import "RTWalkthroughViewController.h"
#import <Google/Analytics.h>
#import "MLAccountRole.h"

@interface MLLoginContoller ()<UITextFieldDelegate, UIAlertViewDelegate,RTWalkthroughViewControllerDelegate>
@property(nonatomic,weak) IBOutlet UITextField*emailTextField;
@property(nonatomic,weak) IBOutlet UITextField*passwordTextField;
@property(nonatomic,weak) IBOutlet UIView*loginView;
@property(nonatomic,weak) IBOutlet UIImageView*logoView;
@property(nonatomic,weak) IBOutlet UIButton*signButton;

@end

@implementation MLLoginContoller

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.loginView.layer.cornerRadius = 10.0;
    self.loginView.layer.borderColor = [UIColor colorFromHexCode:@"#000000" andAlpha:0.2].CGColor;
    self.loginView.layer.borderWidth = 1.0;
    
      self.signButton.enabled = NO;
    
    [self.passwordTextField addTarget:self
                  action:@selector(myTextFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
    [self.emailTextField addTarget:self
                               action:@selector(myTextFieldDidChange:)
                     forControlEvents:UIControlEventEditingChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"login_screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"tutorialBox"]) {
        
        [self presentTutorial];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"tutorialBox"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"touchid"];
        [[NSUserDefaults standardUserDefaults] synchronize];

    }

}

-(IBAction)loginButtonTapped:(id)sender{
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"     // Event category (required)
                                                          action:@"button_press"  // Event action (required)
                                                           label:@"login"          // Event label
                                                           value:nil] build]];    // Event value
    
    if ([self emailIsValid:self.emailTextField.text]&& self.passwordTextField.text.length>4){
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

      [SVProgressHUD showWithStatus:@"Aktualizuji data"];
        
       __block MLSession*session = [[MLSession alloc] init];
        session.password = self.passwordTextField.text;
        session.login = self.emailTextField.text;
        
        [session loginWithBlock:^(BOOL succeeded, NSError *error) {
            
            if (succeeded) {
                
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user_action"     // Event category (required)
                                                                      action:@"did_login"  // Event action (required)
                                                                       label:nil          // Event label
                                                                       value:nil] build]];    // Event value
                
                [[NSUserDefaults standardUserDefaults] setObject:self.passwordTextField.text forKey:@"currentPassword"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                session = [MLSession currentSession];
                
                BOOL canContinue = NO;
                
                for (MLAccountRole *role in session.currentAccount.roles) {
                    
                    if ([role.name isEqualToString:@"manager"]||[role.name isEqualToString:@"single-user"]||[role.name isEqualToString:@"petrol"]) {
                        canContinue = YES;
                        break;
                    }
                    
                }
                
                if (!canContinue) {
                    [[[UIAlertView alloc] initWithTitle:@"Pozor" message:@"K přihlášení je nutné mít v aplikaci Expensa profil Uživatel. Pokud používáte Petrol Card, požádejte o přístup zodpovědnou osobu ve Vaší společnosti." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                    
                    [(MLAppDelegate*)[[UIApplication sharedApplication] delegate] logout];
                    
                    return;
                }

                
                [session updateAccountWithBlock:^(BOOL succeeded, NSError *error) {
                   
                    if (succeeded) {
                        
                        session = [MLSession currentSession];
                        
                        
                        [MLCostCenter fetchAllWithBlock:^(BOOL succeeded, NSError *error) {
                            
                            if (!succeeded) {
                                [self loginError];
                                return;
                            }
                            
                            [MLCurrency fetchAllWithBlock:^(BOOL succeeded, NSError *error) {
                                
                                if (!succeeded) {
                                    [self loginError];
                                    return;
                                }
                                
                                [MLCategory fetchAllWithBlock:^(BOOL succeeded, NSError *error) {
                                    
                                    if (!succeeded) {
                                        [self loginError];
                                        return;
                                    }
                                    
                                    [MLTransaction fetchAllWithBlock:^(BOOL succeeded, NSError *error) {
                                      
                                        if (!succeeded) {
                                            [self loginError];
                                            return;
                                        }
                                        
                                        if (session.currentAccount.isPetrol) {
                                          
                                            [session getPetrolUsersWithBlock:^(BOOL succeeded, NSError *error) {
                                                [SVProgressHUD dismiss];
                                                
                                                [self dismissController];
                                            }];
                                            
                                        }
                                        else{
                                            [SVProgressHUD dismiss];
                                            
                                            [self dismissController];
                                        }
                                        
                                    }];
                                    
                                    
                                
                                    
                                }];

                                
                                
                            }];

                            
                            
                        }];
                        
                        
                        
                        
                      
                        
                        
                     
                        

                    }
                    
                }];
                
                
                
            }
            else{
                
                  [[[UIAlertView alloc] initWithTitle:@"Zadaný e-mail nebo heslo jsou nesprávné." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                self.passwordTextField.text = @"";
                [SVProgressHUD dismiss];   
            }
            
        }];
    }
    else{
        
        [[[UIAlertView alloc] initWithTitle:@"Zadejte e-mail a heslo" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    
    
}

-(void)loginError{
    [SVProgressHUD dismiss];

     [[[UIAlertView alloc] initWithTitle:@"Selhala komunikace se serverem" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    

}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    [(MLAppDelegate*)[[UIApplication sharedApplication] delegate] logout];
  
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    if (textField == self.emailTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    else{
        [self.passwordTextField resignFirstResponder];
    
        if (self.emailTextField.text.length>0 && self.passwordTextField.text.length>0) {
            [self loginButtonTapped:nil];
        }
    
    }
    
    return YES;

}

- (void)myTextFieldDidChange:(id)sender {

    if (self.emailTextField.text.length>0 && self.passwordTextField.text.length>0) {
        self.signButton.enabled = YES;

    }
    else{
        self.signButton.enabled = NO;

    }
    
}



-(IBAction)tutorialButtonTapped:(id)sender{
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"     // Event category (required)
                                                          action:@"button_press"  // Event action (required)
                                                           label:@"tutorial_open"          // Event label
                                                           value:nil] build]];    // Event value

    
    
    UIStoryboard *stb = [UIStoryboard storyboardWithName:@"Walkthrough" bundle:nil];
    RTWalkthroughViewController *walkthrough = [stb instantiateViewControllerWithIdentifier:@"walk"];
    
    RTWalkthroughPageViewController *pageOne = [stb instantiateViewControllerWithIdentifier:@"walk1"];
    RTWalkthroughPageViewController *pageTwo = [stb instantiateViewControllerWithIdentifier:@"walk2"];
    RTWalkthroughPageViewController *pageThree = [stb instantiateViewControllerWithIdentifier:@"walk3"];
    RTWalkthroughPageViewController *pageFour = [stb instantiateViewControllerWithIdentifier:@"walk4"];

    walkthrough.delegate = self;
    [walkthrough addViewController:pageOne];
    [walkthrough addViewController:pageTwo];
    [walkthrough addViewController:pageThree];
    [walkthrough addViewController:pageFour];

    [self presentViewController:walkthrough animated:YES completion:nil];

    
}

-(void)presentTutorial{
    UIStoryboard *stb = [UIStoryboard storyboardWithName:@"Walkthrough" bundle:nil];
    RTWalkthroughViewController *walkthrough = [stb instantiateViewControllerWithIdentifier:@"walk"];
    
    RTWalkthroughPageViewController *pageOne = [stb instantiateViewControllerWithIdentifier:@"walk1"];
    RTWalkthroughPageViewController *pageTwo = [stb instantiateViewControllerWithIdentifier:@"walk2"];
    RTWalkthroughPageViewController *pageThree = [stb instantiateViewControllerWithIdentifier:@"walk3"];
    RTWalkthroughPageViewController *pageFour = [stb instantiateViewControllerWithIdentifier:@"walk4"];
    
    walkthrough.delegate = self;
    [walkthrough addViewController:pageOne];
    [walkthrough addViewController:pageTwo];
    [walkthrough addViewController:pageThree];
    [walkthrough addViewController:pageFour];
    
    [self presentViewController:walkthrough animated:NO completion:nil];
}



- (void)walkthroughControllerDidClose:(RTWalkthroughViewController *)controller {
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"     // Event category (required)
                                                          action:@"button_press"  // Event action (required)
                                                           label:@"tutorial_close"          // Event label
                                                           value:nil] build]];    // Event value
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dismissController {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navController = (UINavigationController *)[storyboard instantiateViewControllerWithIdentifier:@"mainNavigationController"];
    
    [self.view.window setRootViewController:navController];
    
}

-(IBAction)openExpensaWebsite:(id)sender{
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"     // Event category (required)
                                                          action:@"button_press"  // Event action (required)
                                                           label:@"expensa_website"          // Event label
                                                           value:nil] build]];    // Event value
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://expensa.cz/objednat-expensu/"]];
}

-(BOOL)emailIsValid:(NSString*)checkString{
    
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
    
    
}

@end
