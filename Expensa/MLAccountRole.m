//
//  AccountRole.m
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import "MLAccountRole.h"
@implementation MLAccountRole
+(instancetype)roleWithName:(NSString*)name{
    MLAccountRole*role = [[MLAccountRole alloc] init];
    role.name = name;
    return role;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name forKey:@"name"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if((self = [super init])) {
        self.name =  [aDecoder decodeObjectForKey:@"name"];
    }
    return self;
}

@end
