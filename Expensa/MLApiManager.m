//
//  MOApiManager.m
//  uoffice
//
//  Created by Pavel Nemecek on 12/02/14.
//  Copyright (c) 2014 Pavel Nemecek. All rights reserved.
//

#import "MLApiManager.h"
#import "MLUser.h"
#import "Constants.h"
@implementation MLApiManager

+ (instancetype)sharedInstace {
    static id _singleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *baseUrl = [NSURL URLWithString:ApiBaseUrl];
        NSURLSessionConfiguration *configuration;
        configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        
        NSURLCache *cache = [[NSURLCache alloc] initWithMemoryCapacity:10 * 1024 * 1024
                                                          diskCapacity:50 * 1024 * 1024
                                                              diskPath:nil];
        
        [configuration setURLCache:cache];
        
        _singleton = [[self alloc] initWithBaseURL:baseUrl sessionConfiguration:configuration];
    });
    
    return _singleton;
}

-(instancetype)initWithBaseURL:(NSURL *)url sessionConfiguration:(NSURLSessionConfiguration *)configuration{
    
    self = [super initWithBaseURL:url sessionConfiguration:configuration];
    
    if (self) {
        

        
        self.responseSerializer = [MLJSONResponseSerializer serializer];
    }
    
    return self;
}

@end
