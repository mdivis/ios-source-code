//
//  MOSession.h
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLAccount.h"
@interface MLSession : NSObject

@property (nonatomic, strong) NSString *login;
@property (nonatomic, strong) NSString *deviceId;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *pushToken;
@property (nonatomic, strong) NSDate *validUntil;
@property (nonatomic, strong) NSArray*accounts;
@property (nonatomic, strong) MLAccount*currentAccount;
+(instancetype)sessionFromDictionary:(NSDictionary*)dictionary;
+ (instancetype)currentSession;

-(void)loginWithBlock:(void (^)(BOOL succeeded, NSError *error))block;
-(void)logoutWithBlock:(void (^)(BOOL succeeded, NSError *error))block;
-(void)updateAccountWithBlock:(void (^)(BOOL succeeded, NSError *error))block;
-(void)updateBalanceWithBlock:(void (^)(BOOL succeeded, NSError *error))block;

-(void)currentBalanceWithBlock:(void (^)(double balance, NSError *error))block;


-(void)getPetrolUsersWithBlock:(void (^)(BOOL succeeded, NSError *error))block;
- (void)updatePushTokenWithDictionary:(NSDictionary *)userDictionary block:(void (^)(BOOL succeeded, NSError *error))block;
-(void)destroy;
-(void)update;
@end
