//
//  MLTransactionTableViewCell.m
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import "MLTransactionTableViewCell.h"
#import "UIFont+MOFonts.h"
#import "UIColor+MOColors.h"
#import "MLCurrency.h"
#import "MLCategory.h"
@implementation MLTransactionTableViewCell

- (void)awakeFromNib {
    // Initialization code

    self.photoView.layer.cornerRadius = 4.0;
    self.photoView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setupWithTransaction:(MLTransaction*)transaction{

    self.nameLabel.text = transaction.merchant;

    NSDateFormatter*formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd. MM. yyyy";
    
    self.dateLabel.text = [formatter stringFromDate:transaction.date];
    
    NSString*currency = nil;
    NSString*currencyOriginal = nil;

    NSString*amount = nil;
    NSString*amountOriginal = nil;

    MLCurrency*selectedCurrency;
    MLCurrency*originalCurrency;

    RLMResults*results = [MLCurrency objectsWhere:@"currencyId = %@",transaction.currency];
    RLMResults*originalresults = [MLCurrency objectsWhere:@"currencyId = %@",transaction.originalCurrency];

    RLMResults*categoryResults = [MLCategory objectsWhere:@"categoryId = %@",transaction.category];

    
    if (results.count>0) {
        selectedCurrency = results[0];
    }
    
    if (originalresults.count>0) {
        originalCurrency = originalresults[0];
    }
    
    if (categoryResults.count>0) {
        self.categoryLabel.text = [categoryResults[0] name];
    }
    
    if (!transaction.photo) {
        self.photoView.backgroundColor = [UIColor colorFromHexCode:@"#f64932"];
    }
    else{
        self.photoView.backgroundColor = [UIColor greenColor];

    }
    
    
    currency = selectedCurrency.symbol;
    currencyOriginal = originalCurrency.symbol;

    
    
    NSNumberFormatter *currencyFormatter = [MLNumberFormatter sharedInstace];
    
    NSNumberFormatter *currencyFormatter1 = [MLNumberFormaterOriginal sharedInstace];


    
    amount = [currencyFormatter stringFromNumber:@(transaction.priceWithVat)];
    
    if (originalCurrency) {
          [currencyFormatter1 setCurrencyCode:[originalCurrency.currencyId uppercaseString]];
    }
    
    amountOriginal = [currencyFormatter1 stringFromNumber:@(transaction.originalPrice)];


    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",amount]];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorFromHexCode:@"#2ca1ff"] range:NSMakeRange(0,amount.length)];
    [string addAttribute:NSFontAttributeName value:[UIFont mediumFontOfSize:14] range:NSMakeRange(0,amount.length)];

        
    self.amountLabel.attributedText = string;

    self.amountOriginalLabel.hidden = YES;
    
    if (transaction.originalCurrency.length>0) {
        self.amountOriginalLabel.text = [NSString stringWithFormat:@"%@",amountOriginal];
        self.amountOriginalLabel.hidden = NO;

    }

 
}

@end
