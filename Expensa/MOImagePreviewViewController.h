//
//  MOImagePreviewViewController.h
//  Denteractive
//
//  Created by Pavel Nemecek on 10/12/14.
//  Copyright (c) 2014 madeo.mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOImagePreviewViewController : UIViewController<UIScrollViewDelegate>
-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil imageDate:(NSData*)imageData;
@end
