//
//  MOUser.h
//  SimituOFO
//
//  Created by Michal Kovacik on 6/18/14.
//  Copyright (c) 2014 madeo.mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface MLUser : NSObject

@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *avatarURL;
@property (nonatomic, assign) BOOL hasAvatar;
@property (nonatomic,strong) NSData *avatar;

+(instancetype)userFromDictionary:(NSDictionary*)dictionary;
@end