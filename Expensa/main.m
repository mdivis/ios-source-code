//
//  main.m
//  Expensa
//
//  Created by Pavel Nemecek on 27/03/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MLAppDelegate class]));
    }
}
