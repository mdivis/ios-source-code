//
//  MOApiManager.h
//  uoffice
//
//  Created by Pavel Nemecek on 12/02/14.
//  Copyright (c) 2014 Pavel Nemecek. All rights reserved.
//

#import "AFNetworking.h"
#import "MLJSONResponseSerializer.h"

@interface MLApiManager : AFHTTPSessionManager
+ (instancetype)sharedInstace;
@end
