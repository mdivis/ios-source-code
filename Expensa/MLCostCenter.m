//
//  MLCostCenter.m
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import "MLCostCenter.h"
#import "NSDictionary+Value.h"
@implementation MLCostCenter

// Specify default values for properties




+(instancetype)costCenterFromDictionary:(NSDictionary*)dictionary{
    
    MLCostCenter*currency = [[MLCostCenter alloc] init];
    
    currency.costCenterId = [dictionary intValueForKey:@"id" defaultValue:0];
    currency.code = [dictionary stringValueForKey:@"code" defaultValue:@""];
    currency.name = [dictionary stringValueForKey:@"name" defaultValue:@""];
    currency.state = [dictionary stringValueForKey:@"state" defaultValue:@""];
    currency.isPrimary = [dictionary boolValueForKey:@"isPrimary" defaultValue:NO];
    currency.isSecondary = [dictionary boolValueForKey:@"isSecondary" defaultValue:NO];

    
    return currency;
    
}


+(void)fetchAllWithBlock:(void (^)(BOOL succeeded, NSError *error))block{

    NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    [MLApiManager sharedInstace].requestSerializer = [AFHTTPRequestSerializer serializer];
    [[[MLApiManager sharedInstace] requestSerializer] setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];
    [[MLApiManager sharedInstace] GET:ApiConstCenters parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        
        [realm beginWriteTransaction];
        
        [realm deleteObjects:[MLCostCenter allObjects]];
        [realm commitWriteTransaction];

        [realm beginWriteTransaction];
  
        for (NSDictionary*dict in responseObject[@"data"]) {
            [realm addObject:[MLCostCenter costCenterFromDictionary:dict]];
        }
        
  
        [realm commitWriteTransaction];
        
        block(YES, nil);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        block(NO, error);
        
    }];
    

}



@end
