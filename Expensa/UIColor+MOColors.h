//
//  UIColor+UGColors.h
//  Pincamp
//
//  Created by Pavel Nemecek on 8/25/13.
//  Copyright (c) 2013 Unigram. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (MOColors)

+ (UIColor *) navBarBackgroundColor;

+ (UIColor *) navBarTextColor;
+ (UIColor *) headerTextColor;
+ (UIColor *) subHeaderTextColor;
+ (UIColor *) buttonColor;
+ (UIColor *) buttonHighlightColor;
+ (UIColor *) navBarButtonColor;
+ (UIColor *) navBarButtonHighlightColor;
+ (UIColor *) georgiaTextColor;

+(UIColor *)separatorColor;

+ (UIColor *) colorFromHexCode:(NSString *)hexString;
+ (UIColor *) colorFromHexCode:(NSString *)hexString andAlpha:(float)alphaValue;
@end
