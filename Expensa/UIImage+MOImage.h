//
//  UIImage+MOImage.h
//  visionare
//
//  Created by Pavel Nemecek on 12/12/13.
//  Copyright (c) 2013 Madeo mobile&gaming. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (MOImage)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
