//
//  RTCustomPageViewController.h
//  Walkthrough
//
//  Created by Aleksandar Vacić on 4.11.14..
//  Copyright (c) 2014. Radiant Tap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTWalkthroughViewController.h"
#import "RTWalkthroughPageViewController.h"

@interface RTCustomPageViewController : RTWalkthroughPageViewController

@end
