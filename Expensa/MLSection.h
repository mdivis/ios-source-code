//
//  MLSection.h
//  Expensa
//
//  Created by Pavel Nemecek on 19/05/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLCategory.h"
#import "MLTransaction.h"
@interface MLSection : NSObject

@property(nonatomic,strong) NSMutableArray*transactions;
@property (nonatomic,strong) MLCategory*category;

@end
