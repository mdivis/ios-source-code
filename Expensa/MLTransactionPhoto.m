//
//  MLTransactionPhoto.m
//  Expensa
//
//  Created by Pavel Nemecek on 10/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import "MLTransactionPhoto.h"
#import "NSDictionary+Value.h"
@implementation MLTransactionPhoto

+(instancetype)photoFromDictionary:(NSDictionary*)dictionary{

    MLTransactionPhoto*photo = [[MLTransactionPhoto alloc] init];
    photo.transactionId = [dictionary intValueForKey:@"id" defaultValue:0];
    photo.note = [dictionary[@"photo"] stringValueForKey:@"note" defaultValue:@""];
    photo.warning = [dictionary[@"photo"] boolValueForKey:@"warning" defaultValue:NO];
    photo.fileData = [NSData data];
    return photo;
}

@end
