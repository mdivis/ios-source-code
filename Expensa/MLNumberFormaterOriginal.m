//
//  MLNumberFormaterOriginal.m
//  Expensa
//
//  Created by Pavel Nemecek on 20/05/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import "MLNumberFormaterOriginal.h"

@implementation MLNumberFormaterOriginal
+ (instancetype)sharedInstace {
    static id _singleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        _singleton = [[NSNumberFormatter alloc] init];
        [_singleton setNumberStyle:NSNumberFormatterCurrencyStyle];
        [_singleton setCurrencyCode:@"CZK"];
        //    [currencyFormatter setCurrencyCode:@"CZK"];
        [_singleton setMaximumFractionDigits:2];
        [_singleton setMinimumFractionDigits:2];
        
        [_singleton setGroupingSize:3];
        [_singleton setGroupingSeparator:@" "];
        
    });
    
    return _singleton;
}

@end
