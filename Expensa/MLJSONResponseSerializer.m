//
//  MLJSONResponseSerializer.m
//  Expensa
//
//  Created by Pavel Nemecek on 23/11/15.
//  Copyright © 2015 medialabel. All rights reserved.
//

#import "MLJSONResponseSerializer.h"
#import "MLAppDelegate.h"
@implementation MLJSONResponseSerializer

#pragma mark - AFURLResponseSerialization
- (id)responseObjectForResponse:(NSURLResponse *)response
                           data:(NSData *)data
                          error:(NSError *__autoreleasing *)error
{
    id responseObject = [super responseObjectForResponse:response data:data error:error];
     NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if (httpResponse.statusCode == 401)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            //Your main thread code goes in here
            
            MLAppDelegate*delagate = (MLAppDelegate*)[UIApplication sharedApplication].delegate;
            [delagate logout];
        });
   
        
    }
    
    return responseObject;
}


@end
