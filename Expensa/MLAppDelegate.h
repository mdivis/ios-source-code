//
//  AppDelegate.h
//  Expensa
//
//  Created by Pavel Nemecek on 27/03/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BKPasscodeLockScreenManager.h"
@interface MLAppDelegate : UIResponder <UIApplicationDelegate, BKPasscodeLockScreenManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
-(void)logout;
-(void)logoutDismiss;
@property (nonatomic) NSUInteger                failedAttempts;
@property (strong, nonatomic) NSDate            *lockUntilDate;

@property (strong, nonatomic) BKPasscodeViewController *passviewController;
@property(assign, nonatomic) BOOL isReachable;

@end

