//
//  NSDictionary+MOValue.m
//  Denteractive
//
//  Created by Michal Kovacik on 7/17/14.
//  Copyright (c) 2014 madeo.mobile. All rights reserved.
//

#import "NSDictionary+Value.h"
#import "MLDateFormater.h"
@implementation NSDictionary (Value)

- (int)intValueForKey:(NSString *)key {
    return [self intValueForKey:key defaultValue:0];
}

- (int)intValueForKey:(NSString *)key defaultValue:(int)defaultValue {
    return self[key] == [NSNull null]
    ? defaultValue : [self[key] intValue];
}
        
- (NSString *)stringValueForKey:(NSString *)key {
    return [self stringValueForKey:key defaultValue:nil];
}

- (NSString *)stringValueForKey:(NSString *)key defaultValue:(NSString *)defaultValue {
    if (self[key] == nil || self[key] == [NSNull null]) {
        return defaultValue;
    }
    id result = self[key];
    if ([result isKindOfClass:[NSNumber class]]) {
        return [result stringValue];
    }
    return result;
}

- (NSDate *)dateValueForKey:(NSString *)key
{
    if (![self[key] isKindOfClass:[NSNull class]]) {
        
        return  [[MLDateFormater sharedInstace] dateFromString:self[key]];
    }
    else {
        return nil;
    }
}


- (double)doubleValueForKey:(NSString *)key {
    return [self doubleValueForKey:key defaultValue:0];
}

- (double)doubleValueForKey:(NSString *)key defaultValue:(double)defaultValue {
    return self[key] == [NSNull null]
    ? defaultValue : [self[key] doubleValue];
}


- (BOOL)boolValueForKey:(NSString *)key {
    return [self boolValueForKey:key defaultValue:NO];
}

- (BOOL)boolValueForKey:(NSString *)key defaultValue:(BOOL)defaultValue {
    return self[key] == [NSNull null] ? defaultValue
    : [self[key] boolValue];
}

- (NSArray *)arrayValueForKey:(NSString *)key {
    NSObject *obj = self[key];
    if (obj && [obj isKindOfClass:[NSArray class]]) {
        return (NSArray *)obj;
    }
    return nil;
}

@end
