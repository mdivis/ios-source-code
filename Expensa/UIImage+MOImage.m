//
//  UIImage+MOImage.m
//  visionare
//
//  Created by Pavel Nemecek on 12/12/13.
//  Copyright (c) 2013 Madeo mobile&gaming. All rights reserved.
//

#import "UIImage+MOImage.h"

@implementation UIImage (MOImage)

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
