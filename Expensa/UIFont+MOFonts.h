//
//  UIFont+UGFonts.h
//  Pincamp
//
//  Created by Pavel Nemecek on 8/25/13.
//  Copyright (c) 2013 Unigram. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (MOFonts)

+ (UIFont *)regularFontOfSize:(CGFloat)size;
+ (UIFont *)boldFontOfSize:(CGFloat)size;
+ (UIFont *)regularTextFontOfSize:(CGFloat)size;
+ (UIFont *)boldTextFontOfSize:(CGFloat)size;
+ (UIFont *)mediumFontOfSize:(CGFloat)size;

@end
