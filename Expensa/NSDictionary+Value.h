//
//  NSDictionary+MOValue.h
//  Denteractive
//
//  Created by Michal Kovacik on 7/17/14.
//  Copyright (c) 2014 madeo.mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Value)

- (BOOL)boolValueForKey:(NSString *)key;
- (BOOL)boolValueForKey:(NSString *)key defaultValue:(BOOL)defaultValue;
- (int)intValueForKey:(NSString *)key;
- (int)intValueForKey:(NSString *)key defaultValue:(int)defaultValue;
- (NSString *)stringValueForKey:(NSString *)key;
- (NSString *)stringValueForKey:(NSString *)key defaultValue:(NSString *)defaultValue;
- (NSArray *)arrayValueForKey:(NSString *)key;
- (NSDate *)dateValueForKey:(NSString *)key;
- (double)doubleValueForKey:(NSString *)key;
- (double)doubleValueForKey:(NSString *)key defaultValue:(double)defaultValue;

@end
