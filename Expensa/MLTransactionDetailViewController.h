//
//  MLTransactionDetailViewController.h
//  Expensa
//
//  Created by Pavel Nemecek on 03/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLTransaction.h"
#import "BKCurrencyTextField.h"
@interface MLTransactionDetailViewController : UIViewController

@property(nonatomic,strong) MLTransaction*transaction;
@end
