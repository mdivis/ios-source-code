//
//  AppDelegate.m
//  Expensa
//
//  Created by Pavel Nemecek on 27/03/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import "MLAppDelegate.h"
#import "AFNetworkActivityLogger.h"
#import "MLSession.h"
#import "MLLoginContoller.h"
#import <Realm/Realm.h>
#import "UIFont+MOFonts.h"
#import "UIColor+MOColors.h"
#import "UIImage+MOImage.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "MLTransactionsConrtoller.h"
#import "MLTransactionDetailViewController.h"
#import "NSDictionary+Value.h"
#import "FXReachability.h"
#import "JDStatusBarNotification.h"
#import "MLTransaction.h"
#import "MLNewTransactionViewController.h"
#import "MLProfileViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Google/Analytics.h>
@interface MLAppDelegate ()<BKPasscodeViewControllerDelegate>

@end

@implementation MLAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [Fabric with:@[[Crashlytics class]]];

    
        [[BKPasscodeLockScreenManager sharedManager] setDelegate:self];
    [[BKPasscodeLockScreenManager sharedManager] showLockScreen:NO];

   [[AFNetworkActivityLogger sharedLogger] startLogging];
 [[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelDebug];
    
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    // Set the new schema version. This must be greater than the previously used
    // version (if you've never set a schema version before, the version is 0).
    config.schemaVersion = 8;
    
    // Set the block which will be called automatically when opening a Realm with a
    // schema version lower than the one set above
    config.migrationBlock = ^(RLMMigration *migration, uint64_t oldSchemaVersion) {
        // We haven’t migrated anything yet, so oldSchemaVersion == 0
        if (oldSchemaVersion < 1) {
            // Nothing to do!
            // Realm will automatically detect new properties and removed properties
            // And will update the schema on disk automatically
        }
    };
    
    // Tell Realm to use this new configuration object for the default Realm
    [RLMRealmConfiguration setDefaultConfiguration:config];


    if (![MLSession currentSession]) {
        [self showLoginScreen:NO];
    }
    else{
    
    }
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageWithColor:[UIColor colorFromHexCode:@"#333444"]] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[UIImage new] ];
    
    [[UIToolbar appearance] setBackgroundImage:[UIImage imageWithColor:[UIColor colorFromHexCode:@"#333444"]]
                            forToolbarPosition:UIToolbarPositionAny
                                    barMetrics:UIBarMetricsDefault];
    

    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont mediumFontOfSize:18], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:attributes];
    
    NSDictionary *attributesButton = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont regularFontOfSize:16], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    [[UIToolbar appearance] setTintColor:[UIColor whiteColor]];
    [[UIToolbar appearance] setBarTintColor:[UIColor whiteColor]];

    
    [[UIBarButtonItem appearance] setTitleTextAttributes:attributesButton forState:UIControlStateNormal];

 

    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    return YES;
}

-(void)showLoginScreen:(BOOL)animated{
    
    // Get login screen from storyboard and present it
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *viewController = (UINavigationController *)[storyboard instantiateViewControllerWithIdentifier:@"loginViewController"];
    [self.window setRootViewController:viewController];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    
    [[BKPasscodeLockScreenManager sharedManager] showLockScreen:NO];
}



-(void)logoutDismiss{
    

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"avatar"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"passcode"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"shouldShowLockscreen"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
    // Remove data from singleton (where all my app data is stored)
    [[MLSession currentSession] logoutWithBlock:^(BOOL succeeded, NSError *error) {
        [[MLSession currentSession] destroy];
    }];
    

    [self.passviewController dismissViewControllerAnimated:YES completion:nil];
    
    [self showLoginScreen:NO];

    
}


-(void)logout
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"avatar"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"passcode"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"shouldShowLockscreen"];
    [[NSUserDefaults standardUserDefaults] synchronize];


    
    // Remove data from singleton (where all my app data is stored)
    [[MLSession currentSession] logoutWithBlock:^(BOOL succeeded, NSError *error) {
        [[MLSession currentSession] destroy];
    }];
    
      [self showLoginScreen:NO];
    
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}



- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    if (application.applicationIconBadgeNumber>0) {
        application.applicationIconBadgeNumber = 0;
    }
    
  
    
}


- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current Installation and save it to Parse.
    
    
    NSString* newToken = [deviceToken description];
    newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [[NSUserDefaults standardUserDefaults] setObject:newToken forKey:@"lastToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if ([newToken isEqualToString:[MLSession currentSession].pushToken]) {
        
    }
    else{
        [[MLSession currentSession] updatePushTokenWithDictionary:@{@"platform":@"APNS", @"notificationDeviceId":newToken} block:^(BOOL succeeded, NSError *error) {
            
            
            
        }];
    }
    
  
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    
    if (application.applicationState == UIApplicationStateActive) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"updateTransactionsAndBalance" object:nil];

    }
    else{
        
        
        
    }
    
    if (application.applicationState == UIApplicationStateInactive) {
        
        if ([MLSession currentSession]) {

        
        if ([[[userInfo objectForKey:@"aps"] objectForKey:@"event"] objectForKey:@"data"]) {
            
            NSDictionary*transactioin = [[[userInfo objectForKey:@"aps"] objectForKey:@"event"] objectForKey:@"data"];
            
            RLMRealm*realm = [RLMRealm defaultRealm];
            
            
            MLTransaction* trans = [MLTransaction transactionFromDictionary:transactioin];
            
            
            RLMResults*currentTrans = [MLTransaction objectsWhere:@"transactionId = %ld", (long)trans.transactionId];
            
            MLTransaction*transActionToUse = nil;
            
            if (currentTrans.count>0) {
                transActionToUse = [currentTrans firstObject];
                
            }
            else{
                [realm beginWriteTransaction];
                [realm addObject:trans];
                [realm commitWriteTransaction];
                
                transActionToUse = trans;
            }
            
            if ([self.window.rootViewController.presentedViewController isKindOfClass:[MLTransactionDetailViewController class]] || [self.window.rootViewController.presentedViewController isKindOfClass:[MLNewTransactionViewController class]] || [self.window.rootViewController.presentedViewController isKindOfClass:[MLProfileViewController class]]) {
                
                [self.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
                
                
            }
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UINavigationController *navController = (UINavigationController *)[storyboard instantiateViewControllerWithIdentifier:@"mainNavigationController"];
            
            
            MLTransactionDetailViewController *detail = (MLTransactionDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"transDetailViewController"];
            
            detail.transaction = transActionToUse;
            
            
            
            [self.window setRootViewController:navController];
            
            
      [(UINavigationController *)self.window.rootViewController pushViewController:detail animated:NO];
            
            
        }
        
        
        }
        
    }
    
    
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    if (application.applicationState == UIApplicationStateActive) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"updateTransactionsAndBalance" object:nil];
        
    }
    else{
        
        
        
    }
    
    if (application.applicationState == UIApplicationStateInactive) {
        
        if ([MLSession currentSession]) {
            
        
    
        if ([[[userInfo objectForKey:@"aps"] objectForKey:@"event"] objectForKey:@"data"]) {
            
              NSDictionary*transactioin = [[[userInfo objectForKey:@"aps"] objectForKey:@"event"] objectForKey:@"data"];
            
            RLMRealm*realm = [RLMRealm defaultRealm];

            
            MLTransaction* trans = [MLTransaction transactionFromDictionary:transactioin];
            
            
            RLMResults*currentTrans = [MLTransaction objectsWhere:@"transactionId = %ld", (long)trans.transactionId];
            
            MLTransaction*transActionToUse = nil;
            
            if (currentTrans.count>0) {
                transActionToUse = [currentTrans firstObject];
                
            }
            else{
                [realm beginWriteTransaction];
                [realm addObject:trans];
                [realm commitWriteTransaction];
                
                transActionToUse = trans;
            }
            
            
            
     
            
            if ([self.window.rootViewController.presentedViewController isKindOfClass:[MLTransactionDetailViewController class]] || [self.window.rootViewController.presentedViewController isKindOfClass:[MLNewTransactionViewController class]]
                || [self.window.rootViewController.presentedViewController isKindOfClass:[MLProfileViewController class]]
                ) {
                
                [self.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
                
                
            }
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UINavigationController *navController = (UINavigationController *)[storyboard instantiateViewControllerWithIdentifier:@"mainNavigationController"];
            
            
               MLTransactionDetailViewController *detail = (MLTransactionDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"transDetailViewController"];
            
            detail.transaction = transActionToUse;
            
            
            
            [self.window setRootViewController:navController];
            
            
            [(UINavigationController *)self.window.rootViewController pushViewController:detail animated:NO];
            
            
            
            
        }
        }
      
        
    
    }
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)lockScreenManagerShouldShowLockScreen:(BKPasscodeLockScreenManager *)aManager
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults boolForKey:@"shouldShowLockscreen"]) {
        return YES;
    }
    else{
        return NO;
    }
}

- (UIViewController *)lockScreenManagerPasscodeViewController:(BKPasscodeLockScreenManager *)aManager
{
    self.passviewController = [[BKPasscodeViewController alloc] initWithNibName:nil bundle:nil];
    self.passviewController.type = BKPasscodeViewControllerCheckPasscodeType;
    self.passviewController.delegate = self;
    
    
        self.passviewController.touchIDManager = [[BKTouchIDManager alloc] initWithKeychainServiceName:@"com.expensa.keychain.pass"];
        self.passviewController.touchIDManager.promptText = @"Pro ověření naskenujte otisk";
  
  
    
    self.passviewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Odhlásit" style:UIBarButtonItemStylePlain target:self action:@selector(logoutDismiss)];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:self.passviewController];
    return navController;
}

- (void)passcodeViewController:(BKPasscodeViewController *)aViewController authenticatePasscode:(NSString *)aPasscode resultHandler:(void (^)(BOOL))aResultHandler
{
    
    NSString*passcode = [[NSUserDefaults standardUserDefaults] objectForKey:@"passcode"];
    
    if ([aPasscode isEqualToString:passcode]) {
        self.lockUntilDate = nil;
        self.failedAttempts = 0;

        
        aResultHandler(YES);
    } else {
        aResultHandler(NO);
    }
}




- (void)passcodeViewControllerDidFailAttempt:(BKPasscodeViewController *)aViewController
{
    self.failedAttempts++;
    
    if (self.failedAttempts > 4) {
     
        aViewController.view.userInteractionEnabled = NO;
       
        [self logout];
       
        [aViewController dismissViewControllerAnimated:YES completion:nil];

    }
}

- (NSUInteger)passcodeViewControllerNumberOfFailedAttempts:(BKPasscodeViewController *)aViewController
{
    return self.failedAttempts;
}


- (void)passcodeViewController:(BKPasscodeViewController *)aViewController didFinishWithPasscode:(NSString *)aPasscode
{
    //    switch (aViewController.type) {
    //        case BKPasscodeViewControllerNewPasscodeType:
    //        case BKPasscodeViewControllerChangePasscodeType:
    //
    //            [[NSUserDefaults standardUserDefaults]setObject:aPasscode forKey:@"passcode"];
    //            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"shouldShowLockscreen"];
    //            [[NSUserDefaults standardUserDefaults] synchronize];
    //
    //            break;
    //        default:
    //            break;
    //    }
    
    [aViewController dismissViewControllerAnimated:YES completion:nil];
}


@end
