//
//  RTCustomPageViewController.m
//  Walkthrough
//
//  Created by Aleksandar Vacić on 4.11.14..
//  Copyright (c) 2014. Radiant Tap. All rights reserved.
//

#import "RTCustomPageViewController.h"


@implementation RTCustomPageViewController

- (void)viewDidLoad {
	[super viewDidLoad];

}

-(IBAction)closeMaster:(id)sender{

    [self.delegate walkthroughPageRequestsClosing:self];

}

-(IBAction)nextMaster:(id)sender{
    
    [self.delegate walkthroughPageRequestsNext:self];
    
}


@end
