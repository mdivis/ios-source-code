//
//  MLCustomNavigationController.h
//  Expensa
//
//  Created by Pavel Nemecek on 27/10/15.
//  Copyright © 2015 medialabel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MLCustomNavigationController : UINavigationController<UIGestureRecognizerDelegate>

@end
