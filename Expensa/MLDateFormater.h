//
//  MODateFormater.h
//  visionare
//
//  Created by Pavel Nemecek on 06/08/14.
//  Copyright (c) 2014 Madeo mobile&gaming. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MLDateFormater : NSDateFormatter
+ (instancetype)sharedInstace;

@end
