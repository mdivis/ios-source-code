//
//  MLTransactionDetailViewController.m
//  Expensa
//
//  Created by Pavel Nemecek on 03/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import "MLTransactionDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MLCategory.h"
#import "MLCurrency.h"
#import "MLCostCenter.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "SVProgressHUD.h"
#import "AbstractActionSheetPicker.h"
#import "ActionSheetStringPicker.h"
#import "ActionSheetDatePicker.h"
#import "UIImage+ResizeMagick.h"
#import "MOImagePreviewViewController.h"
#import "FXReachability.h"
#import "UIImage+ResizeMagick.h"
#import <Google/Analytics.h>

@interface MLTransactionDetailViewController ()<UITextViewDelegate, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UIAlertViewDelegate>{
    BOOL firstLoad;
    UITextField*currentResponder;
}

@property(nonatomic,weak) IBOutlet BKCurrencyTextField*pyedField;
@property(nonatomic,weak) IBOutlet UITextField*placeField;
@property(nonatomic,weak) IBOutlet BKCurrencyTextField*personalField;
@property(nonatomic,weak) IBOutlet UITextView*noteView;

@property(nonatomic,weak) IBOutlet UIButton*categoryButton;
@property(nonatomic,weak) IBOutlet UIButton*currencyButton;
@property(nonatomic,weak) IBOutlet UIButton*dateButton;
@property(nonatomic,weak) IBOutlet UIButton*expenseCenterButton1;
@property(nonatomic,weak) IBOutlet UIButton*expenseCenterButton2;
@property(nonatomic,weak) IBOutlet UIButton*detailButton;
@property(nonatomic,weak) IBOutlet UIButton*deleteButton;

@property(nonatomic,weak) IBOutlet UILabel*nameLabel;
@property(nonatomic,weak) IBOutlet UILabel*dateLabel;
@property(nonatomic,weak) IBOutlet UIImageView*imageView;


@property(nonatomic,weak) IBOutlet UIButton*photoButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photoHeight;
@property(nonatomic,weak) IBOutlet UIImageView*photoView;

@property (nonatomic,strong) MLCurrency*activeCurrency;
@property (nonatomic,strong) MLCostCenter*activeCostCenter1;
@property (nonatomic,strong) MLCostCenter*activeCostCenter2;
@property (nonatomic,strong) NSDate*activeDate;
@property (nonatomic,strong) MLCategory *activeCategory;

@property (nonatomic,strong) RLMResults *primaryCenters;
@property (nonatomic,strong) RLMResults *secondaryCenters;
@property (nonatomic,strong) RLMResults *currencies;
@property (nonatomic,strong) RLMResults *categories;
@property (nonatomic,strong) RLMResults *selectCategories;

@property(nonatomic,strong) NSTimer *timer;
@property (nonatomic,assign) NSInteger time;


@property (nonatomic,weak) IBOutlet UIScrollView *scrollView;


@end

@implementation MLTransactionDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.time = 0;
    // Do any additional setup after loading the view.
    self.photoButton.layer.cornerRadius = 18.0;
    self.deleteButton.layer.cornerRadius = 18.0;

    [self.pyedField.numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [self.pyedField.numberFormatter setMinimumFractionDigits:2];
    [self.pyedField.numberFormatter setMaximumFractionDigits:2];
    
    
    [self.personalField.numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [self.personalField.numberFormatter setMinimumFractionDigits:2];
    [self.personalField.numberFormatter setMaximumFractionDigits:2];

    
    // Start the notifier, which will cause the reachability object to retain itself!

    
    firstLoad = YES;

    if (self.transaction) {
        
        self.primaryCenters = [MLCostCenter objectsWhere:@"isPrimary = 1"];
        self.secondaryCenters = [MLCostCenter objectsWhere:@"isSecondary = 1"];
        self.selectCategories = [MLCategory objectsWhere:@"categoryId != 'atm'"];
        self.categories = [MLCategory allObjects];
        self.currencies = [[MLCurrency allObjects] sortedResultsUsingProperty:@"symbol" ascending:YES];
        
        if (self.primaryCenters.count<1 ){
            self.expenseCenterButton1.enabled = NO;
            [self.expenseCenterButton1 setTitle:@"Nelze vybrat" forState:UIControlStateNormal];

        }
        if (self.secondaryCenters.count<1 ){
            self.expenseCenterButton2.enabled = NO;
            [self.expenseCenterButton2 setTitle:@"Nelze vybrat" forState:UIControlStateNormal];

        }
        
        if (self.transaction.originalPrice>0) {
            self.pyedField.numberValue = [NSDecimalNumber decimalNumberWithDecimal:[@(self.transaction.originalPrice) decimalValue]];

        }
        else{
          self.pyedField.numberValue = [NSDecimalNumber decimalNumberWithDecimal:[@(self.transaction.priceWithVat) decimalValue]];        }
        
        if (self.transaction.privateSpendingAmount>0) {
             self.personalField.numberValue = [NSDecimalNumber decimalNumberWithDecimal:[@(self.transaction.privateSpendingAmount) decimalValue]];
        }
        
       
        
        self.placeField.text = self.transaction.merchant;
        
        NSDateFormatter*formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd. MM. yyyy";
        
        [self.dateButton setTitle: [formatter stringFromDate:self.transaction.date] forState:UIControlStateNormal];
        
        self.dateLabel.text = [formatter stringFromDate:self.transaction.date];
        self.nameLabel.text = self.transaction.merchant;
        
        UIImage*image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",self.transaction.category]];
        [self.imageView setImage:image];
        
        NSString*currency = nil;
        NSString*currencyOriginal = nil;

        MLCurrency*selectedCurrency;
        MLCurrency*originalCurrency;

        RLMResults*results = [MLCurrency objectsWhere:@"currencyId = %@",self.transaction.currency];
         RLMResults*resultsOriginal = [MLCurrency objectsWhere:@"currencyId = %@",self.transaction.originalCurrency];
        
        RLMResults*categoryResults = [MLCategory objectsWhere:@"categoryId = %@",self.transaction.category];
        
        
        if (results.count>0) {
            selectedCurrency = results[0];
        
        }
        if (resultsOriginal.count>0) {
            originalCurrency = resultsOriginal[0];
            
        }
        
        if (categoryResults.count>0) {
            [self.categoryButton setTitle:[categoryResults[0] name] forState:UIControlStateNormal];
            self.activeCategory = categoryResults[0];

        }
        else{
         [self.categoryButton setTitle:@"Nevybráno" forState:UIControlStateNormal];
        }
        
        self.activeDate = self.transaction.date;
        
        if (originalCurrency) {
            self.activeCurrency = originalCurrency;

        }
        else{
            self.activeCurrency = selectedCurrency;

        }
        
        
        RLMResults*costCenterResults1 = [MLCostCenter objectsWhere:@"costCenterId = %ld",(long)self.transaction.primaryCostCenterId];
        RLMResults*costCenterResults2 = [MLCostCenter objectsWhere:@"costCenterId = %ld",(long)self.transaction.secondaryCostCenterId];

        if (costCenterResults1.count>0) {
            self.activeCostCenter1 = costCenterResults1[0];
            [self.expenseCenterButton1 setTitle:self.activeCostCenter1.name forState:UIControlStateNormal];

        }
        if (costCenterResults2.count>0) {
            self.activeCostCenter2 = costCenterResults2[0];
            [self.expenseCenterButton2 setTitle:self.activeCostCenter2.name forState:UIControlStateNormal];
        
        }
        
     
        self.noteView.text = self.transaction.note;
        
        currency = selectedCurrency.symbol;
        currencyOriginal = originalCurrency.symbol;
        
        if (originalCurrency) {
            [self.currencyButton setTitle:currencyOriginal forState:UIControlStateNormal];

        }
        else{
            [self.currencyButton setTitle:currency forState:UIControlStateNormal];

        }

    
       
        
    }
    
    if (!self.transaction.isEditable) {
     
//        self.view.userInteractionEnabled = NO;
        self.navigationItem.rightBarButtonItem = nil;
        
        self.personalField.enabled = NO;
        self.noteView.userInteractionEnabled = NO;
        self.photoButton.hidden = YES;
        self.categoryButton.enabled = NO;
        
    }
    
    if (!self.transaction.isManual) {
        self.pyedField.enabled = NO;
        self.placeField.enabled = NO;
        self.currencyButton.enabled = NO;
        self.dateButton.enabled = NO;
       self.deleteButton.hidden = YES;

    }
    else{
    
        if (self.transaction.isEditable) {
            self.deleteButton.hidden = NO;
            self.dateButton.enabled = YES;

        }
        
        
    }
    
    if (self.transaction.photo && [FXReachability isReachable]) {
        
        self.photoButton.hidden = NO;
        
        if (self.transaction.photo.fileData.length>0) {
            self.photoView.image = [UIImage imageWithData:self.transaction.photo.fileData];
        }
        else{
            NSString*photLink = [NSString stringWithFormat:@"%@single-user/transactions/%ld/photo",ApiBaseUrl,(long)self.transaction.photo.transactionId];
            
            
            NSMutableURLRequest*request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:photLink] cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:10.0];
            
            
            
            NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                              dataUsingEncoding:NSUTF8StringEncoding];
            
            NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
            [SVProgressHUD setDefaultMaskType: SVProgressHUDMaskTypeBlack];
            [SVProgressHUD showWithStatus:@"Nahrávám foto účtenky"];

            [request setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];
            
            
            [self.photoView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                
                [SVProgressHUD dismiss];
                
                
                if (image) {
                    UIImage*scaledImage = [image resizedImageByWidth:self.view.frame.size.width];
                    self.photoHeight.constant = scaledImage.size.height;
                    self.photoView.image = scaledImage;
                    [[RLMRealm defaultRealm] beginWriteTransaction];
                    self.transaction.photo.fileData = UIImageJPEGRepresentation(image,0.8) ;
                    [[RLMRealm defaultRealm] commitWriteTransaction];
                    
                    
                }
                else{
                    self.photoHeight.constant = 0;

                }
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                [SVProgressHUD dismiss];
                [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
                self.photoHeight.constant = 0;

                [SVProgressHUD showErrorWithStatus:@"Účtenku se nepodařilo nahrát. Zkontrolujte připojení k internetu a zkuste to znova. Pokud je účtenka ve formátu PDF, použijte webovou verzi naší aplikace."];
                
                
                NSLog(@"Error");
            }];
        }
        
        
        
        
        
    }
    
    if (!self.transaction.photo) {
        self.detailButton.hidden = YES;
        self.photoHeight.constant = 0;

    }
    else{
        self.detailButton.hidden = NO;
        
    }
    
 
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"transaction_detail_screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    if (![FXReachability isReachable]) {
        self.navigationItem.rightBarButtonItem = nil;
    }
  
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}


    -(void)viewWillDisappear:(BOOL)animated
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [super viewWillDisappear:animated];
    }
    
    -(void)keyboardWillShow:(NSNotification *)notification
    {
        // self.scrollView.contentSize needs to be set appropriately
        
        if ([self.noteView isFirstResponder]) {
            
    
        
        CGRect keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        
        CGRect visibleArea = self.view.frame;
        visibleArea.size.height -= keyboardRect.size.height;
        CGPoint scrollPoint = CGPointZero;
        
        CGPoint visiblePoint = CGPointMake(0, self.noteView.frame.origin.y + self.noteView.frame.size.height + 70);
        
        if (!CGRectContainsPoint(visibleArea, visiblePoint)) {
            scrollPoint = CGPointMake(0.0, visiblePoint.y - keyboardRect.origin.y);
        }
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, scrollPoint.y, 0.0);
        
        // on iOS 7, the keyboard uses new, undocumented animation curve with integer value of 7.
        // Which is why we can't use block animations here
        // http://stackoverflow.com/questions/18837166/how-to-mimic-keyboard-animation-on-ios-7-to-add-done-button-to-numeric-keyboar
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
        [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        self.scrollView.contentInset = contentInsets;
        self.scrollView.scrollIndicatorInsets = contentInsets;
        self.scrollView.contentOffset = scrollPoint;
        
        [UIView commitAnimations];
                }
        else{
        
            CGRect keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
            
            CGRect visibleArea = self.view.frame;
            visibleArea.size.height -= keyboardRect.size.height;
            CGPoint scrollPoint = CGPointZero;
            
            CGPoint visiblePoint = CGPointMake(0, currentResponder.frame.origin.y + currentResponder.frame.size.height + 70);
            
            if (!CGRectContainsPoint(visibleArea, visiblePoint)) {
                scrollPoint = CGPointMake(0.0, visiblePoint.y - keyboardRect.origin.y);
            }
            
            UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, scrollPoint.y, 0.0);
            
            // on iOS 7, the keyboard uses new, undocumented animation curve with integer value of 7.
            // Which is why we can't use block animations here
            // http://stackoverflow.com/questions/18837166/how-to-mimic-keyboard-animation-on-ios-7-to-add-done-button-to-numeric-keyboar
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
            [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
            [UIView setAnimationBeginsFromCurrentState:YES];
            
            self.scrollView.contentInset = contentInsets;
            self.scrollView.scrollIndicatorInsets = contentInsets;
            self.scrollView.contentOffset = scrollPoint;
            
            [UIView commitAnimations];
        
        }
    }
    
    -(void)keyboardWillHide:(NSNotification *)notification
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
        [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.scrollView.scrollIndicatorInsets = self.scrollView.contentInset;
        
        [UIView commitAnimations];
    }

-(IBAction)categoryButtonTapped:(id)sender{
    [self.view endEditing:YES];

    NSMutableArray*categoriesStrings = [[NSMutableArray alloc] init];
    
    for (MLCategory *category in self.selectCategories) {
        [categoriesStrings addObject:category.name];
    }
    
    [[[ActionSheetStringPicker alloc] initWithTitle:@"Kategorie" rows:categoriesStrings initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        self.activeCategory = self.selectCategories[selectedIndex];
        [self.categoryButton setTitle:selectedValue forState:UIControlStateNormal];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:sender] showActionSheetPicker];
     
     
   

}
-(IBAction)currencyButtonTapped:(id)sender{
    
    [self.view endEditing:YES];

    NSMutableArray*categoriesStrings = [[NSMutableArray alloc] init];
    
    for (MLCurrency *category in self.currencies) {
        [categoriesStrings addObject:category.symbol];
    }
    
    
    int selectedIndex=0;
    
    for (int i =0; i<self.currencies.count; i++) {
        if ([[self.currencies[i] currencyId] isEqualToString:self.activeCurrency.currencyId]) {
            selectedIndex = i;
            break;
        }
    }

    
    [[[ActionSheetStringPicker alloc] initWithTitle:@"Měna" rows:categoriesStrings initialSelection:selectedIndex doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        self.activeCurrency = self.currencies[selectedIndex];
        [self.currencyButton setTitle:self.activeCurrency.symbol forState:UIControlStateNormal];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:sender] showActionSheetPicker];


}
-(IBAction)dateButtonTapped:(id)sender{
    [self.view endEditing:YES];

    ActionSheetDatePicker *picker =

    [[ActionSheetDatePicker alloc] initWithTitle:@"Datum" datePickerMode:UIDatePickerModeDate selectedDate:self.activeDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        self.activeDate=selectedDate;
        NSDateFormatter*formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd. MM. yyyy";
        
        [self.dateButton setTitle: [formatter stringFromDate:selectedDate] forState:UIControlStateNormal];
        

        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:sender];
    
    picker.maximumDate = [NSDate date];
    [picker showActionSheetPicker];
    
}
-(IBAction)expenseCenterButton2Tapped:(id)sender{
    [self.view endEditing:YES];

    
    NSMutableArray*categoriesStrings = [[NSMutableArray alloc] init];
    
    for (MLCostCenter *category in self.secondaryCenters) {
        [categoriesStrings addObject:category.name];
    }
    
    [[[ActionSheetStringPicker alloc] initWithTitle:@"Nákladové centrum 2" rows:categoriesStrings initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        self.activeCostCenter2 = self.secondaryCenters[selectedIndex];
        [self.expenseCenterButton2 setTitle:selectedValue forState:UIControlStateNormal];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:sender] showActionSheetPicker];


}
-(IBAction)expenseCenterButton1Tapped:(id)sender{
    [self.view endEditing:YES];

    
    
    NSMutableArray*categoriesStrings = [[NSMutableArray alloc] init];
    
    for (MLCostCenter *category in self.primaryCenters) {
        [categoriesStrings addObject:category.name];
    }
    
    [[[ActionSheetStringPicker alloc] initWithTitle:@"Nákladové centrum 1" rows:categoriesStrings initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        self.activeCostCenter1 = self.primaryCenters[selectedIndex];
        [self.expenseCenterButton1 setTitle:selectedValue forState:UIControlStateNormal];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:sender] showActionSheetPicker];


}

-(IBAction)photoButtonTapped:(id)sender{

       if ([FXReachability isReachable]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                                 delegate: self
                                                        cancelButtonTitle: @"Zrušit"
                                                   destructiveButtonTitle: nil
                                                        otherButtonTitles: @"Vyfotit", @"Vybrat z uložených", nil];
        [actionSheet showInView:self.view];

    }
    else{
        [[[UIAlertView alloc] initWithTitle:@"Připojení k internetu není dostupné" message:@"V off-line režimu není možné přidávat nebo měnit transakce." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }

    
   

}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self takeNewPhotoFromCamera];
            break;
        case 1:
            [self choosePhotoFromExistingImages];
        default:
            break;
    }
}

- (void)takeNewPhotoFromCamera
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.allowsEditing = NO;
        controller.mediaTypes =  [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
        controller.delegate = self;
        [self.navigationController presentViewController: controller animated: YES completion: nil];
    }
}

-(void)choosePhotoFromExistingImages
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        controller.allowsEditing = NO;
        controller.mediaTypes =  [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
        controller.delegate = self;
        
        [self.navigationController presentViewController: controller animated: YES completion: nil];
    }
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
    
    
    
    
    UIImage *originalImage, *editedImage, *imageToSave;
    
    editedImage = (UIImage *) [info objectForKey:
                               
                               UIImagePickerControllerEditedImage];
    
    originalImage = (UIImage *) [info objectForKey:
                                 
                                 UIImagePickerControllerOriginalImage];
    
    
    
    if (editedImage) {
        
        imageToSave = editedImage;
        
    } else {
        
        imageToSave = originalImage;
        
    }
    
    if (imageToSave) {
        self.detailButton.hidden = NO;
    }
    else{
        self.detailButton.hidden = YES;
        
    }

    
    
        UIImage* resizedImage = [imageToSave resizedImageByMagick: @"800x800"];
        
        [self updateImage:resizedImage];

    
    
}



- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
{
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
}

-(void)updateImage:(UIImage*)image{
    
    self.time = 0;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countTime) userInfo:nil repeats:YES];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user_action"     // Event category (required)
                                                          action:@"updated_transaction_photo"  // Event action (required)
                                                           label:nil          // Event label
                                                           value:nil] build]];    // Event value
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

    [SVProgressHUD showWithStatus:@"Ukládám foto" ];

    UIImage*scaledImage = [image resizedImageByWidth:self.view.frame.size.width];
    self.photoHeight.constant = scaledImage.size.height;
    self.photoView.image = scaledImage;
    
    
    NSData * data = UIImageJPEGRepresentation(image,0.8) ;
    
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@single-user/transactions/%ld/photo", ApiBaseUrl, (long)self.transaction.transactionId] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:data name:@"file" fileName:@"soubor.jpg" mimeType:@"image/jpeg"];
        [formData appendPartWithFormData:[@"soubor" dataUsingEncoding:NSUTF8StringEncoding] name:@"note"];
    } error:nil];
    

    
    NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    
    [request setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];

    
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSProgress *progress = nil;
    
    NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithStreamedRequest:request progress:&progress completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        [SVProgressHUD dismiss];
        if (error) {
            [self.timer invalidate];
            self.timer = nil;
            NSLog(@"Error: %@", error);
           [[[UIAlertView alloc] initWithTitle:@"Při ukládání došlo k chybě." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            
        } else {
            NSLog(@"%@ %@", response, responseObject);
            [self trackUpdate];
            
            
            RLMRealm*realm = [RLMRealm defaultRealm];
            
            MLTransactionPhoto*photo = [[MLTransactionPhoto alloc] init];
            photo.transactionId = self.transaction.transactionId;
            photo.note = @"soubor";
            photo.warning = NO;
            photo.fileData = data;
            
            if (self.transaction.photo) {
                [realm beginWriteTransaction];
                [realm deleteObject:self.transaction.photo];
                [realm commitWriteTransaction];
            }
            
            [realm beginWriteTransaction];
            [realm addObject:photo];
            self.transaction.photo = photo;
            [realm commitWriteTransaction];

            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

                [SVProgressHUD showInfoWithStatus:@"Fotografie uložena" ];
           
            
        }
    }];
    
    [uploadTask resume];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)saveTransaction:(id)sender{
    
    if ([FXReachability isReachable]){
    
    if ([self.pyedField.numberValue doubleValue]<[self.personalField.numberValue doubleValue]) {
        [[[UIAlertView alloc] initWithTitle:@"Osobní útrata nemůže být vyšší než výše samotné platby." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return;
    }
    
    if (self.placeField.text.length<1 || [self.pyedField.numberValue doubleValue]==0) {
        [[[UIAlertView alloc] initWithTitle:@"Zadejte údaje" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return;
    }
    
    if (!self.activeCategory) {
        [[[UIAlertView alloc] initWithTitle:@"Vyberte kategorii" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return;
    }

        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user_action"     // Event category (required)
                                                              action:@"updated_transaction"  // Event action (required)
                                                               label:nil          // Event label
                                                               value:nil] build]];    // Event value


    [self.view endEditing:YES];
    
    RLMRealm*realm=  [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    
    self.transaction.currency = self.activeCurrency.currencyId;
    
    if ([self.activeCurrency.currencyId isEqualToString:@"czk"]) {
        self.transaction.originalCurrency =@"";

    }
    else{
        self.transaction.originalCurrency =self.activeCurrency.currencyId;

    }
    
    
    if (self.placeField.text.length>0) {
        self.transaction.merchant = self.placeField.text;
    }
    if (self.noteView.text.length>0) {
        self.transaction.note = self.noteView.text;
    }
    if (self.pyedField.numberValue.doubleValue>0) {
        self.transaction.priceWithVat = [self.pyedField.numberValue doubleValue];
    }
    if (self.personalField.numberValue.doubleValue>0) {
        self.transaction.privateSpendingAmount = [self.personalField.numberValue doubleValue];
    }
    else{
        self.transaction.privateSpendingAmount = 0;
    }
    self.transaction.date = self.activeDate;
    
    if (self.activeCostCenter1) {
        self.transaction.primaryCostCenterId = self.activeCostCenter1.costCenterId;
    }
    if (self.activeCostCenter2) {
        self.transaction.secondaryCostCenterId = self.activeCostCenter2.costCenterId;
    }
    self.transaction.category = self.activeCategory.categoryId;
    
    [realm commitWriteTransaction];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

    [SVProgressHUD showWithStatus:@"Ukládám data" ];

    [self.transaction updateInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [SVProgressHUD dismiss];

        if (succeeded) {
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

              [SVProgressHUD showInfoWithStatus:@"Transakce uložena" ];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
           [[[UIAlertView alloc] initWithTitle:@"Při ukládání došlo k chybě." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
        
        
    }];
    }
    else{
        [[[UIAlertView alloc] initWithTitle:@"Připojení k internetu není dostupné" message:@"V off-line režimu není možné přidávat nebo měnit transakce." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField{


    
    if (textField == self.placeField) {
        [self.pyedField becomeFirstResponder];

    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    currentResponder = textField;
    
    if (textField==self.pyedField  || textField==self.personalField) {
        
        UITextPosition *beginning = [textField endOfDocument];
        [textField setSelectedTextRange:[textField textRangeFromPosition:beginning
                                                              toPosition:beginning]];

        
        UIToolbar *doneToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        doneToolbar.barStyle = UIBarStyleBlackTranslucent;
        
        
        UIBarButtonItem*doneItem =  [[UIBarButtonItem alloc] initWithTitle:@"Hotovo" style:UIBarButtonItemStyleBordered target:self action:@selector(hideInputView)];
        
        
        doneToolbar.items = [NSArray arrayWithObjects:
                             
                             [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                             doneItem,
                             nil];
        
        
        [doneToolbar sizeToFit];
        
        textField.inputAccessoryView = doneToolbar;
        
    }
    
 
}


-(void)hideInputView{
    [self.view endEditing:YES];


}

- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return NO;
    }else{
        return YES;
    }
}

-(IBAction)deleteButtonTapped{

    if ([FXReachability isReachable]){
        [[[UIAlertView alloc] initWithTitle:@"Opravdu smazat?" message:@"Tato operace je nevratná" delegate:self cancelButtonTitle:@"Ne" otherButtonTitles:@"Ano", nil] show];
    }
    else{
      [[[UIAlertView alloc] initWithTitle:@"Připojení k internetu není dostupné" message:@"V off-line režimu není možné přidávat nebo měnit transakce." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }

    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    if (buttonIndex==1) {
        [self deleteTransaction];
    }

}

-(void)deleteTransaction{
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD showWithStatus:@"Mažu transakci"];

    
    [self.transaction deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
       
        [SVProgressHUD dismiss];
        
        if (succeeded) {
            
            RLMRealm*realm = [RLMRealm defaultRealm];
            
            [realm beginWriteTransaction];
            [realm deleteObject:self.transaction];
            [realm commitWriteTransaction];
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        else{
            [[[UIAlertView alloc] initWithTitle:@"Transakci se nepodařilo smazat" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
        
    }];

  
    
    

}

-(IBAction)detailButtonTapped:(id)sender{

    MOImagePreviewViewController*controller =[[MOImagePreviewViewController alloc] initWithNibName:@"MOImagePreviewViewController" bundle:nil imageDate: UIImageJPEGRepresentation(self.photoView.image,1)];
    
    UINavigationController*nav = [[UINavigationController alloc] initWithRootViewController:controller];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
    
}


-(IBAction)goBack:(id)sender{
    firstLoad = YES;

    [self.navigationController popViewControllerAnimated:YES];

}

-(void)trackUpdate{
    
    [self.timer invalidate];
    self.timer = nil;
    // May return nil if a tracker has not already been initialized with a
    // property ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"photos"                      // Timing category (required)
                                                         interval:@((NSUInteger)(self.time * 1000))   // Timing interval (required)
                                                             name:@"upload"                     // Timing name
                                                            label:nil] build]];
    self.time = 0;
}

-(void)countTime{
    
    self.time++;
    
}

@end
