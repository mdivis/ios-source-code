//
//  MLCategory.h
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//
#import <Foundation/Foundation.h>

#import <Realm/Realm.h>
#import "RLMObject+JSON.h"
#import "MLApiManager.h"
#import "Constants.h"
#import "MLSession.h"

@interface MLCategory : RLMObject
@property NSString*name;
@property NSString*categoryId;

+(void)fetchAllWithBlock:(void (^)(BOOL succeeded, NSError *error))block;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<MLCategory>
RLM_ARRAY_TYPE(MLCategory)
