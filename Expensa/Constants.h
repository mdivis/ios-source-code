//
//  Constants.h
//  Expensa
//
//  Created by Pavel Nemecek on 27/03/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#ifndef Expensa_Constants_h
#define Expensa_Constants_h

static NSString *const CurrentUserKey =@"currentUser";
static NSString *const ApiBaseUrl =@"https://my.expensa.cz/api/";
static NSString *const ApiBaseUrlImage =@"https://my.expensa.cz/api";

static NSString *const ApiUserLogin =@"authentication/login";
static NSString *const ApiUserLogout =@"authentication/logout";
static NSString *const ApiUser =@"single-user/account-detail";
static NSString *const ApiUserAvatar =@"single-user/account-detail/avatar?size=retina";
static NSString *const ApiUserCreateAvatar =@"single-user/account-detail/avatar";
static NSString *const ApiTransActions =@"single-user/transactions";
static NSString *const ApiCategories =@"categories";
static NSString *const ApiConstCenters =@"costs-centers";
static NSString *const ApiCurrency =@"currencies";


#endif
