//
//  MLNumberFormatter.h
//  Expensa
//
//  Created by Pavel Nemecek on 20/05/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MLNumberFormatter : NSNumberFormatter
+ (instancetype)sharedInstace;
@end
