//
//  MLCategory.m
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import "MLCategory.h"

@implementation MLCategory

+(void)fetchAllWithBlock:(void (^)(BOOL succeeded, NSError *error))block{
    
    NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    [MLApiManager sharedInstace].requestSerializer = [AFHTTPRequestSerializer serializer];
    [[[MLApiManager sharedInstace] requestSerializer] setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];
    [[MLApiManager sharedInstace] GET:ApiCategories parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        
        
        NSArray * values = [responseObject allValues];
        
        
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [MLCategory createOrUpdateInRealm:[RLMRealm defaultRealm] withJSONArray:values];
        [realm commitWriteTransaction];
        
        block(YES, nil);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        block(NO, error);
        
    }];
    
    
}
+ (NSDictionary *)JSONInboundMappingDictionary {
    return @{
             @"id": @"categoryId",
             @"label": @"name"
             };
}


+(NSString *)primaryKey{
return @"categoryId";
}

@end
