//
//  Account.h
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLUser.h"
@interface MLAccount : NSObject

@property(nonatomic,assign) NSInteger accountId;
@property(nonatomic,strong) NSString* name;
@property(nonatomic,strong) NSArray*roles;
@property(nonatomic,strong) MLUser*user;
@property(nonatomic) double balance;
@property(nonatomic,assign) BOOL activeCard;
@property(nonatomic,strong) NSString* teamName;
@property(nonatomic,strong) NSString* approveManager;
@property (nonatomic,assign) BOOL isPetrol;
@property(nonatomic,strong) NSArray*petrolUsers;


+(instancetype)accountFromDisctionry:(NSDictionary*)dictionary;

@end
