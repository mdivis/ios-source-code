//
//  MLTransactionPhoto.h
//  Expensa
//
//  Created by Pavel Nemecek on 10/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import <Realm/Realm.h>

@interface MLTransactionPhoto : RLMObject
@property NSInteger transactionId;
@property NSString*note;
@property BOOL warning;

@property NSData* fileData;

+(instancetype)photoFromDictionary:(NSDictionary*)dictionary;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<MLTransactionPhoto>
RLM_ARRAY_TYPE(MLTransactionPhoto)
