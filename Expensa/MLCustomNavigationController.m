//
//  MLCustomNavigationController.m
//  Expensa
//
//  Created by Pavel Nemecek on 27/10/15.
//  Copyright © 2015 medialabel. All rights reserved.
//

#import "MLCustomNavigationController.h"

@interface MLCustomNavigationController ()

@end

@implementation MLCustomNavigationController

- (void)setNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated {
    [super setNavigationBarHidden:hidden animated:animated];
    self.interactivePopGestureRecognizer.delegate = self;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (self.viewControllers.count > 1) {
        return YES;
    }
    return NO;
}


@end
