//
//  MLCurrency.m
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import "MLCurrency.h"
#import "NSDictionary+Value.h"
@implementation MLCurrency


+(instancetype)currencyFromDictionary:(NSDictionary*)dictionary{

    MLCurrency*currency = [[MLCurrency alloc] init];
    
    currency.currencyId = [dictionary stringValueForKey:@"id" defaultValue:@""];
    currency.symbol = [dictionary stringValueForKey:@"symbol" defaultValue:@""];
    
    return currency;

}



+(void)fetchAllWithBlock:(void (^)(BOOL succeeded, NSError *error))block{
    
    NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    [MLApiManager sharedInstace].requestSerializer = [AFHTTPRequestSerializer serializer];
    [[[MLApiManager sharedInstace] requestSerializer] setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];
    [[MLApiManager sharedInstace] GET:ApiCurrency parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        
        
        NSArray * values = [responseObject allValues];
        

        
        RLMRealm *realm = [RLMRealm defaultRealm];
        
        [realm beginWriteTransaction];
        [realm deleteObjects:[MLCurrency allObjects]];
        [realm commitWriteTransaction];
        
        [realm beginWriteTransaction];
        
        for (NSDictionary*ditct in values) {
            [realm addObject:[MLCurrency currencyFromDictionary:ditct]];
        }
        
        [realm commitWriteTransaction];
        
        block(YES, nil);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        block(NO, error);
        
    }];
    
    
}


@end
