//
//  MLTransaction.h
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "MLApiManager.h"
#import "Constants.h"
#import "MLSession.h"
#import "MLTransactionPhoto.h"
@interface MLTransaction : RLMObject

@property NSInteger transactionId;
@property NSString*currency;
@property NSString*originalCurrency;

@property NSString*category;
@property NSDate*date;
@property double fxDifference;
@property BOOL isManual;
@property NSString*merchant;
@property NSString*note;
@property NSInteger primaryCostCenterId;
@property NSInteger secondaryCostCenterId;
@property double originalPrice;
@property double privateSpendingAmount;
@property double priceWithVat;
@property NSString*state;
@property BOOL isEditable;

@property MLTransactionPhoto*photo;

+(instancetype)transactionFromDictionary:(NSDictionary*)dictionary;
+(void)fetchAllWithBlock:(void (^)(BOOL succeeded, NSError *error))block;

-(void)createInBackgroundWithBlock:(void (^)(BOOL succeeded, NSError *error))block;
-(void)updateInBackgroundWithBlock:(void (^)(BOOL succeeded, NSError *error))block;
-(void)deleteInBackgroundWithBlock:(void (^)(BOOL succeeded, NSError *error))block;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<MLTransaction>
RLM_ARRAY_TYPE(MLTransaction)
