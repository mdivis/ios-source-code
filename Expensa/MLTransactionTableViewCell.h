//
//  MLTransactionTableViewCell.h
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLTransaction.h"
#import "MLNumberFormatter.h"
#import "MLNumberFormaterOriginal.h"
@interface MLTransactionTableViewCell : UITableViewCell

@property(nonatomic,weak) IBOutlet UILabel*nameLabel;
@property(nonatomic,weak) IBOutlet UILabel*amountOriginalLabel;
@property(nonatomic,weak) IBOutlet UILabel*amountLabel;
@property(nonatomic,weak) IBOutlet UILabel*dateLabel;
@property(nonatomic,weak) IBOutlet UIView*photoView;
@property(nonatomic,weak) IBOutlet UILabel*categoryLabel;

-(void)setupWithTransaction:(MLTransaction*)transaction;

@end
