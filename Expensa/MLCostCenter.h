//
//  MLCostCenter.h
//  Expensa
//
//  Created by Pavel Nemecek on 02/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//
#import <Foundation/Foundation.h>

#import <Realm/Realm.h>
#import "RLMObject+JSON.h"
#import "MLApiManager.h"
#import "Constants.h"
#import "MLSession.h"
@interface MLCostCenter : RLMObject
@property NSInteger costCenterId;
@property NSString*code;
@property NSString*name;
@property NSString*state;
@property BOOL isPrimary;
@property BOOL isSecondary;

+(void)fetchAllWithBlock:(void (^)(BOOL succeeded, NSError *error))block;

+(instancetype)costCenterFromDictionary:(NSDictionary*)dictionary;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<MLCostCenter>
RLM_ARRAY_TYPE(MLCostCenter)
