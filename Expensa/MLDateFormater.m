//
//  MODateFormater.m
//  visionare
//
//  Created by Pavel Nemecek on 06/08/14.
//  Copyright (c) 2014 Madeo mobile&gaming. All rights reserved.
//

#import "MLDateFormater.h"

@implementation MLDateFormater

+ (instancetype)sharedInstace {
    static id _singleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _singleton = [[NSDateFormatter alloc] init];
        [_singleton setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        [_singleton setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    });
    return _singleton;
}


@end
