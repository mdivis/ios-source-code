//
//  MOUser.m
//  SimituOFO
//
//  Created by Michal Kovacik on 6/18/14.
//  Copyright (c) 2014 madeo.mobile. All rights reserved.
//

#import "MLUser.h"
#import "NSDictionary+Value.h"
#import "MLApiManager.h"
#import "MLSession.h"
@implementation MLUser



+(instancetype)userFromDictionary:(NSDictionary*)dictionary
{
    
    MLUser*user = [[MLUser alloc] init];
    
    user.userId =  [dictionary intValueForKey:@"id"];
    user.firstName = [dictionary stringValueForKey:@"firstName"];
    user.lastName = [dictionary stringValueForKey:@"lastName"];
    user.email = [dictionary stringValueForKey:@"email"];
    user.avatarURL = [dictionary[@"avatarLink"] stringValueForKey:@"retina"];
    user.fullName = [dictionary stringValueForKey:@"fullName"];
    user.hasAvatar = [dictionary boolValueForKey:@"hasAvatar"];

    return user;
}



- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeInteger:self.userId forKey:@"userId"];
    [aCoder encodeObject:self.firstName forKey:@"firstName"];
    [aCoder encodeObject:self.lastName forKey:@"lastName"];
    [aCoder encodeObject:self.email forKey:@"email"];
    [aCoder encodeObject:self.avatarURL forKey:@"avatarURL"];
    [aCoder encodeObject:self.fullName forKey:@"fullName"];
    [aCoder encodeBool:self.hasAvatar forKey:@"hasAvatar"];
    [aCoder encodeObject:self.avatar forKey:@"avatar"];

    
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if((self = [super init])) {
        self.userId =  [aDecoder decodeIntForKey:@"userId"];
        self.firstName = [aDecoder decodeObjectForKey:@"firstName"];
        self.lastName = [aDecoder decodeObjectForKey:@"lastName"];
        self.email = [aDecoder decodeObjectForKey:@"email"];
        self.fullName = [aDecoder decodeObjectForKey:@"fullName"];
        self.avatarURL = [aDecoder decodeObjectForKey:@"avatarURL"];
        self.hasAvatar = [aDecoder decodeBoolForKey:@"hasAvatar"];
        self.avatar = [aDecoder decodeObjectForKey:@"avatar"];

    }
    return self;
}


@end
