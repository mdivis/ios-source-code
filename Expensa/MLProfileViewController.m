//
//  MLProfileViewController.m
//  Expensa
//
//  Created by Pavel Nemecek on 03/04/15.
//  Copyright (c) 2015 medialabel. All rights reserved.
//

#import "MLProfileViewController.h"
#import "UIColor+MOColors.h"
#import "MLSession.h"
#import "MLAppDelegate.h"
#import "Constants.h"
#import "UIImage+ResizeMagick.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "MLApiManager.h"
#import "BKPasscodeViewController.h"
#import <Google/Analytics.h>

@interface MLProfileViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIActionSheetDelegate, UIAlertViewDelegate, BKPasscodeViewControllerDelegate>

@property (nonatomic,weak) IBOutlet UIButton*logoutButton;
@property (nonatomic,weak) IBOutlet UIButton*blockButton;

@property(nonatomic,weak) IBOutlet UIButton*avatarButton;
@property(nonatomic,weak) IBOutlet UIImageView*avatarImageView;

@property(nonatomic,weak) IBOutlet UILabel*nameLabel;
@property(nonatomic,weak) IBOutlet UILabel*teamLabel;
@property(nonatomic,weak) IBOutlet UILabel*managerLabel;
@property(nonatomic,weak) IBOutlet UISwitch*pinSwitch;
@property(nonatomic,weak) IBOutlet UISwitch*balanceSwitch;
@property(nonatomic,weak) IBOutlet UISwitch*chronoSwitch;
@property(nonatomic,weak) IBOutlet UILabel*versionLabel;
@property(nonatomic,weak) IBOutlet UISwitch*touchIDSwitch;



@end

@implementation MLProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    self.versionLabel.text =[NSString stringWithFormat:@"Verze %@",currentVersion];

    
    self.logoutButton.layer.borderColor = [UIColor colorFromHexCode:@"#cacdd9"].CGColor;
    self.logoutButton.backgroundColor = [UIColor colorFromHexCode:@"#c3c6d2" andAlpha:0.2];
    self.logoutButton.layer.borderWidth = 1;
    self.logoutButton.layer.cornerRadius = self.blockButton.layer.cornerRadius = 4;
    self.avatarImageView.layer.cornerRadius = 45;
    
    MLSession*sesion = [MLSession currentSession];
    
    if (sesion.currentAccount.isPetrol) {
        self.avatarButton.hidden = YES;
        [self.avatarImageView setImage:[UIImage imageNamed:@"petrol"]];
    }
    else{
        [self.avatarImageView setImage:[UIImage imageNamed:@"avatar"]];

    }
    
    
    NSString*avatartLink = [NSString stringWithFormat:@"%@single-user/account-detail/avatar?size=retina",ApiBaseUrl];
    
    if (sesion.currentAccount.user.hasAvatar) {
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"avatar"]) {
            self.avatarImageView.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"avatar"]];
        }
        else{
            NSMutableURLRequest*request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:avatartLink] cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:1.0];
            
            
            
            NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                              dataUsingEncoding:NSUTF8StringEncoding];
            
            NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
            
            
            [request setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];
            
            
            [self.avatarImageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                
                if (image) {
                    self.avatarImageView.image = image;
                    
                    [[NSUserDefaults standardUserDefaults] setObject:UIImageJPEGRepresentation(image, 1) forKey:@"avatar"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                }
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                
            }];
        }
        
          }
    self.nameLabel.text = sesion.currentAccount.user.fullName;
    self.managerLabel.text =[NSString stringWithFormat:@"Schvaluje %@",sesion.currentAccount.approveManager];
    self.teamLabel.text = sesion.currentAccount.teamName;

    

    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"profile_screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"shouldShowLockscreen"]) {
        [self.pinSwitch setOn:YES];
        self.touchIDSwitch.enabled = YES;

    }
    else{
        [self.pinSwitch setOn:NO];
        self.touchIDSwitch.enabled = NO;


    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"hideBalance"]) {
        [self.balanceSwitch setOn:NO];
    }
    else{
        [self.balanceSwitch setOn:YES];
        
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"chrono"]) {
        [self.chronoSwitch setOn:YES];
    }
    else{
        [self.chronoSwitch setOn:NO];
        
    }
    
    
    BKTouchIDManager*manager = [[BKTouchIDManager alloc] initWithKeychainServiceName:@"com.expensa.keychain.pass"];
    
    if (manager.touchIDEnabled) {
        [self.touchIDSwitch setOn:YES];
    }
    else{
        [self.touchIDSwitch setOn:NO];
        
    }
    
  
}

-(IBAction)pinSwitchValueChanged:(UISwitch*)sender{
    
    if (!self.pinSwitch.isOn) {
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"     // Event category (required)
                                                              action:@"switch"  // Event action (required)
                                                               label:@"pin_off"          // Event label
                                                               value:nil] build]];    // Event value
        
        UIAlertView*allert =   [[UIAlertView alloc] initWithTitle:@"Chcete vypnout PIN?" message:@"Vypnutím PIN ochrany pro vstup do aplikace přebíráte veškerou zodpovědnost za zneužití aplikace." delegate:self cancelButtonTitle:@"Ne" otherButtonTitles:@"Ano", nil] ;
        allert.tag = 888;
        [allert show];
        
    }
    else{
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"     // Event category (required)
                                                              action:@"switch"  // Event action (required)
                                                               label:@"pin_on"          // Event label
                                                               value:nil] build]];    // Event value
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"shouldShowLockscreen"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.touchIDSwitch.enabled = YES;

        
    }

}

-(IBAction)balanceSwitchValueChanged:(UISwitch*)sender{
    
    if (!self.balanceSwitch.isOn) {
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"     // Event category (required)
                                                              action:@"switch"  // Event action (required)
                                                               label:@"hide_ballance_on"          // Event label
                                                               value:nil] build]];    // Event value
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hideBalance"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    else{
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"     // Event category (required)
                                                              action:@"switch"  // Event action (required)
                                                               label:@"hide_ballance_off"          // Event label
                                                               value:nil] build]];    // Event value
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"hideBalance"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
}

-(IBAction)chronoSwitchValueChanged:(UISwitch*)sender{
    
    if (self.chronoSwitch.isOn) {
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"     // Event category (required)
                                                              action:@"switch"  // Event action (required)
                                                               label:@"chrono_on"          // Event label
                                                               value:nil] build]];    // Event value
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"chrono"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    else{
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"     // Event category (required)
                                                              action:@"switch"  // Event action (required)
                                                               label:@"chrono_off"          // Event label
                                                               value:nil] build]];    // Event value
        
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"chrono"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
}

-(IBAction)touchSwitchValueChanged:(UISwitch*)sender{
        BKTouchIDManager*manager = [[BKTouchIDManager alloc] initWithKeychainServiceName:@"com.expensa.keychain.pass"];
    if (self.touchIDSwitch.isOn) {
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"     // Event category (required)
                                                              action:@"switch"  // Event action (required)
                                                               label:@"touch_id_on"          // Event label
                                                               value:nil] build]];    // Event value
        
        [manager savePasscode:[[NSUserDefaults standardUserDefaults] objectForKey:@"passcode"] completionBlock:^(BOOL success) {
            
        }];
   
      
        
    }
    else{
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"     // Event category (required)
                                                              action:@"switch"  // Event action (required)
                                                               label:@"touch_id_off"          // Event label
                                                               value:nil] build]];    // Event value
        
       [manager deletePasscodeWithCompletionBlock:^(BOOL success) {
           
       }];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)dismiss:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(IBAction)logoutButtonTapped:(id)sender{
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"     // Event category (required)
                                                          action:@"button_press"  // Event action (required)
                                                           label:@"logout"          // Event label
                                                           value:nil] build]];    // Event value
    
    UIAlertView*allert =   [[UIAlertView alloc] initWithTitle:@"Opravdu odhlásit?" message:nil delegate:self cancelButtonTitle:@"Ne" otherButtonTitles:@"Ano", nil] ;
    allert.tag = 333;
    [allert show];




}

-(IBAction)blockButtonTapped:(id)sender{
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"     // Event category (required)
                                                          action:@"button_press"  // Event action (required)
                                                           label:@"block_card"          // Event label
                                                           value:nil] build]];    // Event value

 UIAlertView*allert =   [[UIAlertView alloc] initWithTitle:@"Opravdu blokovat kartu?" message:@"Tato akce je nevratná!" delegate:self cancelButtonTitle:@"Ne" otherButtonTitles:@"Ano", nil] ;
    allert.tag = 999;
    [allert show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    if (buttonIndex == 1 && alertView.tag == 999) {
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user_action"     // Event category (required)
                                                              action:@"did_blocked_card"  // Event action (required)
                                                               label:nil          // Event label
                                                               value:nil] build]];    // Event value
        
        NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                          dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
        
        [MLApiManager sharedInstace].requestSerializer = [AFHTTPRequestSerializer serializer];
        [[[MLApiManager sharedInstace] requestSerializer] setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];
        
        
        [[MLApiManager sharedInstace] PUT:@"single-user/card?action=block" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            
             [[[UIAlertView alloc] initWithTitle:@"Karta byla zablokována" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];            
         
            
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"LOGIN ERROR %@",error.description);
            
        }];

        
        
    }
    
      if (buttonIndex == 1 && alertView.tag == 888) {
          [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"shouldShowLockscreen"];
          [[NSUserDefaults standardUserDefaults] synchronize];
          
          
            BKTouchIDManager*manager = [[BKTouchIDManager alloc] initWithKeychainServiceName:@"com.expensa.keychain.pass"];
          
          [manager deletePasscodeWithCompletionBlock:^(BOOL success) {
              
          }];
          
          [self.touchIDSwitch setOn:NO animated:NO];
          self.touchIDSwitch.enabled = NO;

      }
    
       if (buttonIndex == 0 && alertView.tag == 888){
           [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"shouldShowLockscreen"];
           [[NSUserDefaults standardUserDefaults] synchronize];
           [self.pinSwitch setOn:YES];
           self.touchIDSwitch.enabled = YES;

           [self changeNewPasscode:nil];

      }
    
    if (buttonIndex ==1 && alertView.tag == 333) {
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user_action"     // Event category (required)
                                                              action:@"did_logout"  // Event action (required)
                                                               label:nil          // Event label
                                                               value:nil] build]];    // Event value
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"avatar"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [(MLAppDelegate*)[[UIApplication sharedApplication] delegate] logout];
    }

}

-(IBAction)photoButtonTapped:(id)sender{
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"     // Event category (required)
                                                          action:@"button_press"  // Event a (required)
                                                           label:@"avatar"          // Event label
                                                           value:nil] build]];    // Event value

    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: @"Zrušit"
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: @"Vyfotit", @"Vybrat z uložených", nil];
    [actionSheet showInView:self.view];


}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self takeNewPhotoFromCamera];
            break;
        case 1:
            [self choosePhotoFromExistingImages];
        default:
            break;
    }
}

- (void)takeNewPhotoFromCamera
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.allowsEditing = YES;
        controller.mediaTypes =  [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
        controller.delegate = self;
        [self.navigationController presentViewController: controller animated: YES completion: nil];
    }
}

-(void)choosePhotoFromExistingImages
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        controller.allowsEditing = YES;
        
        controller.mediaTypes =  [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
        controller.delegate = self;
        
        [self.navigationController presentViewController: controller animated: YES completion: nil];
    }
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
    
    
    
    
    UIImage *originalImage, *editedImage, *imageToSave;
    
    editedImage = (UIImage *) [info objectForKey:
                               
                               UIImagePickerControllerEditedImage];
    
    originalImage = (UIImage *) [info objectForKey:
                                 
                                 UIImagePickerControllerOriginalImage];
    
    
    
    if (editedImage) {
        
        imageToSave = editedImage;
        
    } else {
        
        imageToSave = originalImage;
        
    }
    
    
    
        UIImage* resizedImage = [imageToSave resizedImageByMagick: @"800x800"];
        
        
        [self updateUserAvatarWithImage:resizedImage];
        

    
    
    
}



- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
{
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
}

-(void)updateUserAvatarWithImage:(UIImage*)image{
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user_action"     // Event category (required)
                                                          action:@"did_update_avatar"  // Event action (required)
                                                           label:nil          // Event label
                                                           value:nil] build]];    // Event value
    
    [self.avatarImageView setImage:image];
    
    NSData * data = UIImageJPEGRepresentation(image,0.8) ;
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"avatar"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@", ApiBaseUrl, @"single-user/account-detail/avatar"] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:data name:@"file" fileName:@"soubor.jpg" mimeType:@"image/jpeg"];
    } error:nil];
    
    NSData *nsdata = [[NSString stringWithFormat:@"%ld:%@", (long)[MLSession currentSession].currentAccount.accountId,[MLSession currentSession].token]
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    
    [request setValue:[NSString stringWithFormat:@"Basic %@",base64Encoded] forHTTPHeaderField:@"Authorization"];

    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSProgress *progress = nil;
    
    NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithStreamedRequest:request progress:&progress completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            NSLog(@"%@ %@", response, responseObject);
            
//            [[MLSession currentSession] updateAccountWithBlock:^(BOOL succeeded, NSError *error) {
//                
//            }];
            
        }
    }];
    
    [uploadTask resume];
    
}

-(IBAction)changePasscode:(id)sender{
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"     // Event category (required)
                                                          action:@"button_press"  // Event action (required)
                                                           label:@"change_passcode"          // Event label
                                                           value:nil] build]];    // Event value

    BKPasscodeViewController *viewController = [[BKPasscodeViewController alloc] initWithNibName:nil bundle:nil];
    viewController.delegate = self;
    viewController.type = BKPasscodeViewControllerChangePasscodeType;
    // viewController.type = BKPasscodeViewControllerChangePasscodeType;    // for change
    // viewController.type = BKPasscodeViewControllerCheckPasscodeType;   // for authentication
    
    viewController.passcodeStyle = BKPasscodeInputViewNumericPasscodeStyle;
    // viewController.passcodeStyle = BKPasscodeInputViewNormalPasscodeStyle;    // for ASCII style passcode.
    
    // To supports Touch ID feature, set BKTouchIDManager instance to view controller.
    // It only supports iOS 8 or greater.
    viewController.touchIDManager = [[BKTouchIDManager alloc] initWithKeychainServiceName:@"com.expensa.keychain.pass"];
    viewController.touchIDManager.promptText = @"Pro ověření naskenujte otisk";   // You can set prompt text.
    
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Zrušit" style:UIBarButtonItemStylePlain target:self action:@selector(passcodeViewCloseButtonPressed:)];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:NO completion:nil];

}

-(IBAction)changeNewPasscode:(id)sender{
    
    BKPasscodeViewController *viewController = [[BKPasscodeViewController alloc] initWithNibName:nil bundle:nil];
    viewController.delegate = self;
    viewController.type = BKPasscodeViewControllerNewPasscodeType;
    // viewController.type = BKPasscodeViewControllerChangePasscodeType;    // for change
    // viewController.type = BKPasscodeViewControllerCheckPasscodeType;   // for authentication
    
    viewController.passcodeStyle = BKPasscodeInputViewNumericPasscodeStyle;
    // viewController.passcodeStyle = BKPasscodeInputViewNormalPasscodeStyle;    // for ASCII style passcode.
    
    // To supports Touch ID feature, set BKTouchIDManager instance to view controller.
    // It only supports iOS 8 or greater.
    viewController.touchIDManager = [[BKTouchIDManager alloc] initWithKeychainServiceName:@"com.expensa.keychain.pass"];
    viewController.touchIDManager.promptText = @"Pro ověření naskenujte otisk";   // You can set prompt text.
    
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Zrušit" style:UIBarButtonItemStylePlain target:self action:@selector(passcodeViewCloseButtonPressed:)];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:NO completion:nil];
    
}

- (void)passcodeViewCloseButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (void)passcodeViewController:(BKPasscodeViewController *)aViewController didFinishWithPasscode:(NSString *)aPasscode
{
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user_action"     // Event category (required)
                                                          action:@"did_change_passcode"  // Event action (required)
                                                           label:nil          // Event label
                                                           value:nil] build]];    // Event value
    
    switch (aViewController.type) {
        case BKPasscodeViewControllerNewPasscodeType:
            
            [[NSUserDefaults standardUserDefaults]setObject:aPasscode forKey:@"passcode"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"shouldShowLockscreen"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            
        case BKPasscodeViewControllerChangePasscodeType:
            
            [[NSUserDefaults standardUserDefaults]setObject:aPasscode forKey:@"passcode"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"shouldShowLockscreen"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            break;
        default:
            break;
    }
    
    [aViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)passcodeViewController:(BKPasscodeViewController *)aViewController authenticatePasscode:(NSString *)aPasscode resultHandler:(void (^)(BOOL))aResultHandler
{
    
    NSString*passcode = [[NSUserDefaults standardUserDefaults] objectForKey:@"passcode"];
    
    if ([aPasscode isEqualToString:passcode]) {
        
        
        aResultHandler(YES);
    } else {
        aResultHandler(NO);
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
